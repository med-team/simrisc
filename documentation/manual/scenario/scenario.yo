tt(Scenario) class objects are defined by tt(Analysis) class objects and
contain parameter values of the simulation to perform. For each separate
analysis a new tt(Scenario) object is constructed.

Most of its members are em(accessors): tt(const) members returning values of
scenario parameters.

Some parameter values are stored in the tt(Scenario) object itself. Refer to
the bf(simrisc)(1) manual page for a description of their default values:
    itemization(
    itt(d_iterations), the number of iterations to perform (defaults to 1);
    itt(d_labels), a vector of strings containing the values of tt(label:)
        lines that may be specified in tt(analysis:) sections to briefly
        describe the essence of simulations;
    itt(d_nWomen), the number of cases to simulate (defaults to 100). This
        name was used in the original versions of the s() program, and may be
        changed to tt(d_nCases) in future versions. For now it is kept;
    itt(d_seed), the initial seed of for the fixed-seed or increasing-seed
        random number generator. See the tt(Random) section for details about
        how random values are gegenerated.
    itt(d_spread), when set to tt(true) some (otherwise fixed) configuration
        parameters may show random fluctuations. Cf. chapter ref(SPREAD) for
        details.
    )

Configuration parameters start with identifying names, like tt(costs:) or
tt(screeningRounds:). Those names are then followed by the parameter's
specifications. Those specifications are made available by the members
    itemization(
    itt(value), returning the value of a numeric parameter;
    itt(lines), containing iterators to the lines in a tt(ConfFile) identified
        by the identifying name;
    itt(find), returning the iterator to a single line identified by the
        identifying name.
    )
