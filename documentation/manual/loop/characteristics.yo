The function tt(Loop:characteristics) may be called during the pre-screening
phase and during the post-screening phase.

In both phases the tumor is self-detected, the tumor characteristics are
determined (tt(Tumor::characteristics, Tumor::setDeathAge), cf. sections
ref(TUMORCHARACT) and ref(TUMORDEATHAGE)), and the treatment costs are
determined using the tumor's induced death age and the tumor's diameter
(tt(Costs::treatment), cf. section ref(COSTS)).

If the case's natural death occurs before the tumor would have caused the
case's death then the case leaves the pre-screening or post-screening
simulation with status (respectively) tt(LEFT_PRE) and
tt(LEFT_POST). Otherwise death was caused by the tumor and the the case leaves
the pre-screening or post-screening simulation with status (respectively)
tt(TUMOR_PRE) and tt(TUMOR_POST).
