COMMENT(

manpagesection(Changes introduced in version 15.01.00)

    itemization(
    it()
    )

manpagesection(Changes introduced in version 15.00.00)

    itemization(
    it() In addition to breast cancer simulations sr() can also perform lung
        cancer simulations for either men or women. By default breast cancer
        simulations are performed.
    it() The configuration file was reorganized and extended with parameters
        used when performing lung cancer simulations: specifications for
        tt(CT) scans; additional Tumor tt(Beir7, Growth, Incidence,) and
        tt(Survival) parameters, and a new section tt(S3) containing the
        age-range specific probabilities of encountering metastases.
    it() Sr() offers the new option tt(--cancer) specifying the type of
        simulation.
    )

When performing lung cancer simulations only the tt(CT) modality can be
specified. When performing breast cancer simulations the tt(CT) modality
cannot be specified, but as with previous versions: any combination of the
tt(Mammo, Tomo) and tt(MRI) modalities can be specified.

manpagesection(Changes introduced in version 14.04.00)

    itemization(
    it() Parameters affected by tt(spread: true)nl()
       Parameters that may vary are specified using triplets: value, spread
        and distribution. In all cases the spread values and distribution
        names are optional: they can both be omitted or both must be
        specified. If these parameters are not specified then their tt(value)
        parameter won't vary if tt(spread: true) is specified;
    it() The tt(Mammo, Tomo,) and tt(MRI) modalities are provided with std.dev
        and distribution parameters for their tt(Dose, M, Beta, Specificity,)
        and tt(Sensitivity) parameters;
    it()  When tt(spread: true) is specified the actually used and original
        parameter values are listed in a file, by default tt(spread-$.txt),
        where tt($) is replaced by the loop iteration index. Use the option
        tt(-s) to specify a non-default filename (cf. bf(simrisc)(1));
    it() Age ranges no longer have trailing colons;
    it() The Case-specific data matrix defines an extra (18th) column, showing
        the results of the screening rounds for each simulated case;
    it() The order of the tt(beir7 beta) and tt(eta) parameters is reversed:
        tt(eta) is specified first, followed by tt(beta). The tt(spread) and
        tt(distribution) parameters following tt(beta) apply to tt(beta), and
        not to tt(eta), which is a fixed value.
    )
END COMMENT)
