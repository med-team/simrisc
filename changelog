simrisc (16.02.00)

  * Natural deaths occurring before tumor onsets are no longer using
    'UNDETECTED_XXX' exit codes but 'NATURAL_XXX' exit codes.

  * In those cases 'Yes' status flags are changed to 'No'

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Thu, 15 Jun 2024 11:33:25 +0200

simrisc (16.01.00)

  * The lung cancer survival parameters (i.e., table S4) received an e
    (quality of medical support) parameter, like the BC e-parameter.

  * The simriscparams(7) man-page was updated accordingly

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Wed, 05 Jun 2024 09:35:24 +0200

simrisc (16.00.00)

  * Reimplemented major elements of the simulation algorithm: elements of the
    pre-screening, intervals between screening rounds and post-screening are
    not separately defined anymore, but all call the same implementation
    components.

  * Simplified handling of the screening rounds and the post-screening step. 
    Pre-screen now superfluous. For details: sse README.loop-tests

  * Simrisc's organization was reorganized avoiding multiply implemented
    elements deriving from versions before simrisc 12.00.00.

  * The organization of the configuration file and of Analysis specifications
    was simplified.

  * Screening::writeParameters reports the attendanceRate in 2 decimals

  * Growth parameters are specified using millimeters, not anymore using
    ln(millimeters)

  * In the data*txt files case indices use natural numbers (starting at 1)

  * The data*txt files show the death status legend below the time-stamp and
    analysis label 

  * Type e (quality of the medical care) was added to the survival: type:
    specifications.

  * Tumor incidence parameters (probabilities) can also be specified using a
    table, in addition to using distribution parameters (cf. Incidence in the
    simriscparams(7) man-page).

  * Man-pages were updated.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Wed, 29 May 2024 17:37:41 +0200

simrisc (15.05.00)

  * natural death ages < tumor death ages are no longer reported as death by
    tumors 

  * repaired sudden increases in # tumor cases 

  * avoiding tumor sizes < growth.start sizes

  * the name 'detectableAge' is confusing: renamed to 'onset', indicating the
    tumor's start

  * interval cancers prevent exact 5mm tumor occurrences, caused by spurious
    onset values

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Mon, 29 Jan 2024 12:40:08 +0100

simrisc (15.04.00)

  * The params.txt file also contains the cumulative death-proportions if
    specified (currently: with lung-cancer simulations)

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Thu, 07 Dec 2023 21:10:54 +0100

simrisc (15.03.01)

  * When reading the cumulative death proportions errors in the specification
    of the cumulative death proportions are checked (like lines broken at a
    final '='-character).

  * Changed the copyright to the Gnu GPL

  * Cosmetics on the simrisc.xref

  * The c++std file is no longer used. Instead the ICMAKE_CXXFLAGS environment
    variable is inspected.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 28 Oct 2023 15:08:19 +0200
  
simrisc (15.03.00)

  * Repaired survival/cptvsdrow: d_vsdRow was set to S3's row instead of
    S4's element index, which also resulted in errors in computing, e.g., the
    number of interval cancers. Because of this bug Simrisc before 15.03.00
    should not anymore be used for lung cancer simulations. Using Simrisc for
    breast cancer simulations is OK.

  * When using lung cancer simulations the parameters written to file now
    correctly report the lung cancer parameters.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 01 Jul 2023 15:34:15 +0200

simrisc (15.02.00)

  * Costs values are now stored in size_t instead of in uint16_t variables 

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Wed, 14 Jun 2023 16:22:36 +0200

simrisc (15.01.00)

  * Added a column TNM to the data file containing the TNM categories per
    case. With breast cancer the 2nd value carries no meaning and is set to 0.

  * Added specification 'bc:' to the default configuration file defining the
    tumor diameter boundaries when using breast cancer simulations.

  * The S3 table in the default configuration specifies increasing (inclusive)
    boundaries for lung cancer tumer diameters.

  * Added option --tnm inserting -1,0 in the TNM column of the data file for
    cases not having tumors.

  * Fixed handling CT sensitivity computations (in the simulations percentages
    instead of proportions were used).

  * Fixed the CT specificity mean (was 99.2, changed to .992).

  * Added the class Log producing extensive logging when handling cases.

  * Added option --log "begin end logfile [set] " to log process information
    for the selected case-index ranges.

  * Fixed a boundary bug in probgroup/indexof.cc causing indexOf() to return
    an index value exceeding the width of a cumulative probability vector if
    the generated probability equals 1.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Mon, 08 May 2023 10:33:36 +0200

simrisc (15.00.00)

  * Added option --cancer (-c) which is used to specify the simulation
    type. By default '--cancer breast' is used, in which case breast cancer
    simulations are performed. Alternatively, '--cancer female' can be
    specified in which case lung cancer simulations are performed for women,
    and '--cancer male' can be specified in which case lung cancer simulations
    are performed for men.

  * When performing lung-cancer simulations modality CT must be used (default
    costs, dose, sensitivity and specificity parameters are defined in the
    default configuration file in section 'Modalities:' 'CT:').

  * Added option --cum-death (-d) which is used to specify the name of a file
    containing the 101 cumulative death proportions.

  * Default proportions are provided in the default configuration file at the
    'death:' parameter specification of the section 'Scenario:'.

  * Option --err is still present, but is removed in a future version. It is
    not mentioned in the current man-page and in simrisc's usage information
    anymore.

  * The default configuration file contains parameters used with lung cancer
    simulations for
        Costs: Discount: proportion:,
        Tumor: Growth: start:,
        Tumor: Growth: lung: (used to specify the selfDetect parameters, for
    breast cancer simulations Tumor: Growth: breast: is used)
        Tumor: Growth: DoublingTime: lung:
    And separate Male: and Female: subsections for Tumor: Incidence: (for
    breast cancer simulation Tumor: Incidence: Breast: is used)
        Survival: lung[0-3]: sections to determine the survival parameters, in
    combination with the new section Tumor: S3: (see the simriscparams(7)
    man-page for details).

  * The simrisc(1) and simriscparams(7) manpages were updated.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 10 Dec 2022 12:25:10 +0100

simrisc (14.05.01)

  * Ready for libbobcat6

  * Added 'c++std' defining the c++ standard to use for
    compilation. Compilation commands also use -Werror

  * Renamed the 'oxref' option in the 'build' script to 'xref'

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 17 Sep 2022 16:07:04 +0200

simrisc (14.05.00)

  * Added option --err (-e) to select the previously used but inorrect
    algorithm for computing the Beir7 risk vector, instead of using the
    (now default) correct algorithm for computing the risk vector.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 22 May 2021 22:09:08 +0200

simrisc (14.04.00)

  * Modalities support spread-variation. See the simriscparams(7) man-page for
    a description of how the configuration file was extended wrt the
    Modality-specifications.  When 'spead: true' is specified then
    Loop::iterate() now calls d_modalities.vary(), calling ModBase vary for
    the various Modalities.  Previously defined 'double' Modality values now
    use VSDs

  * Age range specifications do not end in ':'s anymore. E.g.,
            ageGroup:  1 - 50

  * Beir7's parameter specification first specifies eta, then beta.

  * The Case-specific data matrix defines an extra (18th) colum, showing
    the results of the screening rounds for each simulated case.

  * When 'spread: true' is specified the actually used and original parameter
    values are listed in a file, by default 'spread-$.txt', where $ is
    replaced by the loop iteration index. Use the option '-s' to specify a
    non-default filename (cf. simrisc(1));

  * The man-pages were update

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sun, 11 Apr 2021 11:00:58 +0200

simrisc (14.03.00)

  * To specify no screening round ages 'round: none' should be specified. When
    'round: none' is specified then the rounds file is not written.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sun, 21 Feb 2021 13:30:38 +0100

simrisc (14.02.00)

  * Option --cases was renamed to --last-case (-l) to avoid confusion with
    the cases: specification in the configuration file

  * Default option values are reset at each Analysis: specification.

  * Analysis files now use (capitalized) Analysis: specifications.

  * Fixed some specification errors in the simriscparams(7) man-page

  * Updated the simrisc(1) man-page

  * Updated the usage info

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Wed, 27 Jan 2021 16:39:02 +0100

simrisc (14.01.00)

  * 'm:' parameter specifications for the four bi-rad categories were added to
    the Mammo specifications in the default configuration file

  * The 'm:' parameter corresponding to the cases bi-rad category given the
    case's age is used when computing the sensitivity associated with the
    Mammo modality

  * The simriscparams(7) man-page was updated accordingly

  * The simrisc --help info lists the --cases (-c) option instead of --nCases
    (-n).

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Fri, 22 Jan 2021 11:14:22 +0100

simrsic (14.00.00)

  * Case-specific data are reproducible when specifying 'generator: fixed'
    or 'generator: increasing'

  * Parameter variation when specifying 'spread: true' is available

  * The organization of the configuration file was drastically modified

  * The validity of parameter value specifications is checked

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Wed, 20 Jan 2021 20:43:53 +0100

simrsic (13.03.00)

  * Redefined variables in the Costs class: the discount percentage in fact is
    a proportion, and its type was adapted accordingly

  * MRI simulation was incorrectly implemented in the original program. After
    fixing this error handling False Negatives is now using a standard
    procedure for all Modalities

  * The configuration file and the simriscparams man-page were adapted
    accordingly.

  * The classes Options, Random, and Error are now singletons.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Tue, 27 Oct 2020 19:56:12 +0100

simrsic (13.02.01)

  * Added a Copyright-notice file

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Mon, 12 Oct 2020 09:34:04 +0200

simrsic (13.02.00)

  * Specifications using multiple analyses no longer overwrite existing files

  * Added specification 'generator' to the scenario specifications to specify
    the way the random number generator is initialized

  * The classes Pool, IDpool, and RandomPool are superfluous

  * MRI handling is activated.

  * The loop Status enum now uses 'PRESENT' instead of 'ALIVE' and Status enum
    values beginning with 'LEFT_' instead of 'NATURAL_'

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Tue, 29 Sep 2020 16:04:26 +0200

simrsic (13.01.00)

  * Added missing registration of d_roundDetected to loop/maybedetect.cc

  * loop/selfdetected.cc and loop/treatmentdeath.cc are identical except for
    used Status values: now inline, calling new function in
    loop/characteristics.cc

  * Added a new section OUTPUT to the simrisc(3) man-page, containing
    descriptions of the content of generated files

  * By default the id-base option uses value 1



simrsic (13.00.00-pre)

  * Reorganized config/simrisc, costs associated with Modalities are now
    defined by the individual modality-specifications

  * Repaired the computation of d_prePeriod in tumor/reset.cc

  * Implemented Danielle's code and modified the handling of multiple
    modalities

  * Standardized the handling of Modalities by defining one standard interface

  * Updated the man-pages

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Thu, 16 Jul 2020 13:45:32 +0200

simrsic (12.03.01)

  * Dropped 'oneAnalysis' and 'showAll' from Options, added specificAges(),
    fixing a bug in handling specific ages / N-cases specifications

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Fri, 29 May 2020 15:52:30 +0200

simrsic (12.03.00)

  * Fixed handling of filename specifications in analysis: sections

  * The --one-analysis option no longer requires command-line arguments

  * Analysis: specifications may be empty

  * Minor update of the man-page

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Fri, 29 May 2020 13:39:29 +0200

simrsic (12.02.02)

  * Changed the content of /usr/share/doc/simrisc: it now contains the
    README.hierarchy file and the default configuration file (simrisc.gz)

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Tue, 26 May 2020 13:38:10 +0200

simrsic (12.02.01)

  * Updated the short option letters of the program's usage info

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Tue, 26 May 2020 11:14:14 +0200

simrsic (12.02.00)

  * Added option --id-base to use id-based random numbers instead of
    random-pool based random numbers.

  * All options referring to filesystem entries use capitalized single letter
    options; all other options are lowercase

  * Updated the man-pages.

  * The default parameter specification file is available as the file
    'simrisc' in the distribution's .config/ directory

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Mon, 25 May 2020 15:48:49 +0200

simrsic (12.01.01)

  * New sub-minor release due to a man-page bug.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 23 May 2020 21:20:04 +0200

simrsic (12.01.00)

  * Reorganized the class hierarchy, preparing for multi-threading: see
    README.hierarcy.

  * Base directories are created if not available

  * Output files are provided with a timestamp and optionally analysis labels

  * Output files are cleaned up: improved table formats, and CSVs for the data
    files

  * The file echoing the used parameters is by default not produced. The
    --parameters option is used to specify the name of that file if requested.

  * When specifying configuration parameters in the context of an analysis
    either all, or none of the parameters of a category must be provided. This
    procedure is used to allow reduction of, e.g., screening rounds.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 23 May 2020 19:26:48 +0200

simrsic (12.00.00)

  * Following a request by Marcel Greuter (m.j.w.greuter@umcg.nl) SimRiSc was
    completely rewrittin in this release. Earlier specific changelog
    version information is not available. In the distribution earlier
    changelog info is available under 'legacy'

  * This is the initial conversion, offering the basic facilities.
    The current program produces the same results as the original program,
    for a single scenario, but specifying 100'000 women is OK.

  * Lots of work is still required to update the man-pages, and to completely
    generalize the program.

  * Debian package construction is (locally) operational.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sun, 17 May 2020 17:47:06 +0200
