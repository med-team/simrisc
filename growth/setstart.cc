//#define XERR
#include "growth.ih"

void Growth::setStart()
{
    d_base.back() = "start:";

    LineInfo const *lineInfo = Parser::one(d_base).get();
    istringstream in{ lineInfo->tail };            // get the line's tail

    if (not (in >> d_start))
    {
        Err::msg(Err::SPEC_ERROR) << 
                "[Growth:] start: must specify at least one value" << endl;
        return;
    }

    if (not Globals::isBreast())                // use a 2nd value for LC
    {
        double lcStart;
        if (in >> lcStart)
            d_start = lcStart;
    }

    if (d_start < 1)                            // start value must be >= 1
        Err::atLeast(1);
}
