//#define XERR
#include "growth.ih"

void Growth::writeParameters(ostream &out) const
{
    VSD::fmt(1, 2, 0, 3);               // used at selfDetect

    out << "    Growth:\n"
           "      diameters:\n" <<
           setw(8) << ' ' << "start:                 " <<
                                        setw(4) << d_start << '\n' <<
           setw(8) << ' ' << "self-detect std.dev.:  " <<
                                        setw(4) << exp(d_selfSD) << '\n' <<
           setw(8) << ' ' << "self-detect mean:      " <<
                                        d_selfMean << '\n' <<
           setw(6) << ' ' << "DoublingTime:\n";

    if (d_bc)
    {
        AgeGroupVSD::fmt(8, 1, 2, 0, 2);
        out << d_doublingTime << '\n';
    }
    else
    {
        auto const &first = d_doublingTime.front();
        double sd = first.stdDev();
        
        out << setw(8) << ' ' << "value: " << exp(first.value()) << '\n';
        if (sd >= 0)
            out << setw(8) << ' ' << "std.dev: " << exp(sd) << '\n';

        out.put('\n');
    }
}
