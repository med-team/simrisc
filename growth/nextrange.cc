//#define XERR
#include "growth.ih"

bool Growth::nextRange(AgeGroupVSD const &next) const
{
    return d_doublingTime.empty()
           or
           next.beginAge() == d_doublingTime.back().endAge();
}
