inline AgeGroupVSD const &Growth::ageGroupVSD(uint16_t idx) const
{
    return (this->*d_ageGroupVSD)(idx);
}

inline double Growth::start() const
{
    return d_start;
}

inline VSD const &Growth::selfMu() const
{
    return d_selfMean;
}

inline double Growth::selfSigma() const
{
    return d_selfSD;
}
