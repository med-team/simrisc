#define XERR
#include "growth.ih"

void Growth::setSelfDetect()
{
                                                    // extract the selfdetect
                                                    // specifications
    d_base.back() = Globals::isBreast() ? "breast:" : "lung:";

    Parser::extract(d_base, d_selfSD, d_selfMean);

    if (0 <= d_selfSD)                          // OK if selfSD is positive
    {
        d_selfSD = log(d_selfSD);
        d_selfMean.ln();
        return;
    }

    Err::negative();
    d_selfSD = 0;
}
