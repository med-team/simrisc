#define XERR
#include "growth.ih"

void Growth::setDoublingTime()
{
    d_base.back() = "DoublingTime:";
    size_t size = d_base.size();

    if (not Globals::isBreast())                        // used for LC sim.s
    {
        d_base.push_back("lung:");

        AgeGroupVSD spec{ VARY_NONNEG, true, false };   // useStdDev and don't
                                                        // extract Group
        Parser::extract(d_base, spec);
        spec.ln();

        d_doublingTime.push_back(spec);

        d_ageGroupVSD = &Growth::ageGroupVSDlung;
        d_base.resize(size);
        return;
    }


    d_base.push_back("ageGroup:");
    auto lines = Parser::any(d_base);

    bool push = true;
    while (true)
    {
        LineInfo const *line = lines.get();

        if (line == 0)
            break;

        AgeGroupVSD spec{ VARY_NONNEG, true };  // do extract Group
        if (not Parser::extract(*line, spec))
            push = false;

        if (not push)
            continue;                           // continue for syntax-check
                                                // purposes

        if (not spec.group().nextRange(d_doublingTime))
        {
            push = false;
            continue;
        }

        spec.ln();

        d_doublingTime.push_back(spec);
    }

    d_ageGroupVSD = &Growth::ageGroupVSDbreast;
    d_base.resize(size);
}

