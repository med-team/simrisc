#ifndef INCLUDED_GROWTH_
#define INCLUDED_GROWTH_

// Tumor:
//    Growth:
//            #  breast   lung
//        start:    5       3
//
//        #selfDetect:      # stdev       mean    spread  dist
//        breast:             .70         2.92    .084    Normal
//        lung:               .014        3.037   .061    Normal
//
//        DoublingTime:
//            #                   stdev   mean  spread  dist.
//            ageGroup:  1 - 50:   .61    4.38   .43    Normal
//            ageGroup: 50 - 70:   .26    5.06   .17    Normal
//            ageGroup: 70 - * :   .45    5.24   .23    Normal
//
//            #     all ages      stdev   mean  spread  dist.
//            lung:                .21    4.59   .74    Normal

#include "../agegroupvsd/agegroupvsd.h"

class VSD;

class Growth
{
    StringVect d_base;

    bool    d_bc;               // not --cancer: breast was specified
    double  d_start;

    double  d_selfSD;           // SD of d_selfMean's mean value
    VSD     d_selfMean;         // mean and SD/Distr. used when 'vary'is set

    AgeGroupVSDvect d_doublingTime;
    AgeGroupVSD const &(Growth::*d_ageGroupVSD)(uint16_t idx) const;

    public:
        Growth();

        AgeGroupVSD const &ageGroupVSD(uint16_t idx) const;             // .f

        void vary(std::ostream &out);

        double start() const;                                           // .f
        VSD const &selfMu() const;                                      // .f
        double selfSigma() const;                                       // .f

        void writeParameters(std::ostream &out) const;

    private:
        AgeGroupVSD const &ageGroupVSDbreast(uint16_t idx) const;
        AgeGroupVSD const &ageGroupVSDlung(uint16_t idx) const;

        void setStart();
        void setSelfDetect();
        void setDoublingTime();

        bool nextRange(AgeGroupVSD const &next) const;


        static void writeDiameter(std::ostream &out, char const *name,
                                  VSD const &vsd);
//         static void writeDT(std::ostream &out, char const *name,
//                             size_t idx, double value);
};

#include "growth.f"

#endif
