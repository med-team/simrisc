//#define XERR
#include "modbase.ih"

    // used by MRI and CT
void ModBase::extractBase(VSD &dest)
{
    auto lines = Parser::one(d_base);        // get one line

    LineInfo const *line = lines.get();

    if (line != 0)
        Parser::extract(*line, dest);
}
