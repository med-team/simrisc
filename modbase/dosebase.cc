#define XERR
#include "modbase.ih"

void ModBase::doseBase(VSDvect &dose)
{
    d_base.back() = "dose:";
    dose = Parser::VSDparameters(VARY_NONNEG, d_base, 4);
}
