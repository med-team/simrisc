#define XERR
#include "modbase.ih"

//  #             ageGroup
//  specificity:  0 - 40:  .961     40 - *: .965

void ModBase::specificityBase(AgeGroupVSDvect &dest)
{
    d_base.back() = "specificity:";

    auto lines = Parser::one(d_base);

    if (lines.size() != 1)                      // keywords not found
        return;

    istringstream in{ lines.get()->tail };      // prepare for extraction

    bool nextRange = false;

    while (true)
    {
        AgeGroupVSD spec{ VARY_PROB, false };   // agegroupvsd w/o sd.

        if (not (in >> spec))                   // try to extract
            break;                              // no (more)

        nextRange = spec.group().nextRange(dest);
//FBB           spec.ageGroup().nextRange(dest))

        if (not nextRange)
            break;

        dest.push_back(move(spec));

        if (spec.group().end() == END_AGE)
            return;
    }

   if (not nextRange)
       Err::specification();
}
