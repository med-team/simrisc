#ifndef INCLUDED_MODBASE_
#define INCLUDED_MODBASE_

// see ../modalities/README

#include <iomanip>
#include <unordered_map>

#include "../err/err.h"
#include "../typedefs/typedefs.h"
#include "../agegroupvsd/agegroupvsd.h"

class ModBase
{
    friend std::ostream &operator<<(std::ostream &, ModBase const &);

    StringVect d_base;

    std::string const d_id;

    bool d_defined;

    size_t d_cost = 0;                        // cost of using th modality
    SizeVect d_count;
    uint16_t d_falsePositives;

    public:
        ModBase(std::string const &id);

        virtual ~ModBase();

        size_t cost() const;                                        // .f
        void count(size_t round);                                   // .f
        bool defined() const;                                       // .f
        double dose(uint16_t idx) const;                            // .f
        void falsePositive();                                       // .f
        std::string const &id() const;                              // .f
        size_t operator[](size_t round) const;                      // .f
        void resetCounters(size_t nRounds);
        double sensitivity(size_t idx) const;                       // .f

        double specificity(double age) const;                       // .f
        void vary(std::ostream &out);        // see Growth::vary()  // .f

    protected:
        StringVect &base();                                         // .f

        void doseBase(VSDvect &dose);

        void extractBase(VSD &dest);                // used by MRI and CT

                                    // ageGroup and specificity
                                    // sets keywords: [2]: Specificity
                                    //                [3]: ageGroup
        void specificityBase(AgeGroupVSDvect &dest);

    private:
                                                // vDose members return 0
        virtual VSDvect const *vDose() const;                       // 1
        virtual double vDose(uint16_t idx) const;                   // 2

        virtual void vInsert(std::ostream &out) const = 0;
        virtual double vSensitivity(size_t idx) const = 0;
        virtual double vSpecificity(double age) const = 0;
        virtual void vVary(std::ostream &out) = 0;

        static void outSpec(std::ostream &out, char const *prefix,
                            unsigned fill, AgeGroupVSD const &spec);
};

// protected void costBase(StringVect &keywords);   sets d_cost from "costs:"


#include "modbase.f"

#endif
