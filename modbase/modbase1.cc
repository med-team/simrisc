//#define XERR
#include "modbase.ih"

ModBase::ModBase(string const &id)
:
    d_base{ id + ':', "costs:" },
    d_id(id),
    d_defined(Parser::hasSection(d_base))
{
    if (d_defined)
        Parser::nonNegative(d_base, d_cost);
}
