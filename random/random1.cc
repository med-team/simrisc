#define XERR
#include "random.ih"

Random::Random()
:
    d_nEngines(Globals::vary() ? N_DISTRIBUTIONS : N_STD_DISTRIBUTIONS),
    d_engine(d_nEngines)
{
    // xerr("vary = " << Globals::vary());

    if (not Globals::vary())
        return;

    d_normalVary.reset(new normal_distribution<>{});
    d_uniformVary.reset(new uniform_real_distribution<>{});
    d_logNormalVary.reset(new lognormal_distribution<>{});

}
