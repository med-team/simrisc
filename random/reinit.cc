#define XERR
#include "random.ih"

// Called from Loop::iterate, defines the betadistributions if vary is set

void Random::reinit(size_t nCases, GeneratorType type)
{
    if (Globals::vary() and nCases == 1)
        throw Exception{} << "'spread: true' cannot be used with 'cases: 1'";

    d_nCases = nCases;

    switch (type)
    {
        case INCREASING_SEED:
            ++d_seed;
        break;

        case FIXED_SEED:
        break;

        default:
            d_seed = time(0);
        break;
    }

    reset();
}
