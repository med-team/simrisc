//#define XERR
#include "random.ih"

double Random::uniform()
{
    double ret = d_uniform(d_engine[UNIFORM]);
    g_log('U') << "          uniform random value: " << ret << nl;
    return ret;
}
