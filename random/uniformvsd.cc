#define XERR
#include "random.ih"

double Random::uniformVSD()
{
    double ret = d_uniformCase(d_engine[UNIFORM_VSD]);
    g_log('V') << "          uniform vsd random value: " << ret << nl;
    return ret;
}
