//#define XERR
#include "random.ih"

double Random::logNormalVary(double mean, double stdDev)
{
    d_logNormalVary->param(LogNormalParams{ mean, stdDev });

    return (*d_logNormalVary)(d_engine[LOGNORMAL_VARY]);
}
