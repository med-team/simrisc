#define XERR
#include "random.ih"

void Random::reset()
{
    size_t seed = d_seed;

    for (size_t idx = 0; idx != N_STD_DISTRIBUTIONS; ++idx)
    {
        d_engine[idx] = mt19937{seed <<= 1};
//        xerr(seed);
    }

    d_uniform.reset();              // clear the generators' caches
    d_uniformCase.reset();
    d_uniformVSD.reset();
    d_logNormal.reset();

    if (Globals::vary())            // clear the varying distributions' caches
    {
        seed <<= 1;
        d_engine[NORMAL_VARY] = mt19937{ seed };
        d_normalVary->reset();

        seed <<= 1;
        d_engine[UNIFORM_VARY] = mt19937{ seed };
        d_uniformVary->reset();

        seed <<= 1;
        d_engine[LOGNORMAL_VARY] = mt19937{ seed };
        d_logNormalVary->reset();

        seed <<= 1;
        d_engine[UNIFORM_VARY] = mt19937{ seed };
        d_uniformVSDvary->reset();

        if (d_betaVary)
        {
            d_engine[BETA_VARY] = mt19937{ d_seed };    // std seed is used
            d_betaVary->reset();   // reset the beta distr.'s cache
        }
    }
}
