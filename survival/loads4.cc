#define XERR
#include "survival.ih"

void Survival::loadS4()
{
    for (char const *column: s_lungColumn)
    {
        d_vsdMatrix.push_back(              // read 4 VSD values in a
                    Parser::VSDparameters(  // VSDvector and push that vector
                        VARY_MEAN,          // to the vsdMatrix
                        { column },
                        { 'a', 'b', 'c', 'd', 'e' }
                    )
                );
//        xerr("d_vsdMatrix " << column);
//        for (auto value: d_vsdMatrix.back())
//            cerr << value.value() << ' ';
//        cerr << '\n';
    }
}
