#define XERR
#include "survival.ih"

void Survival::writeLung(ostream &out) const
{
    lungParameters(out);
    s3parameters(out);
}
