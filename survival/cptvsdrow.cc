#define XERR
#include "survival.ih"

    // determine the row of d_vsdMatrix to use by locating the row in s3
    // containing 'diameter' in its ProbGrooup.end() value. Then compute
    // the lowest column index in that row exceeding a random proportion
    // value. That index becomes d_vsdRow.
RowCol Survival::cptVSDrow(double diameter)
{
    // if diameter > 100 the last line in S3 must be used. probIndexOf uses
    // 101 as end-value because that's an end-age and probGroups are normally
    // based on ages.
    // Therefore, if diameter > 100 100 is passed to probIndexOf
    // resulting in the last line of S3 being used.

                // S3's row (first) and S3's column (second)
    RowCol rowCol = ProbGroup::probIndexOf(
                                    d_s3,
                                    diameter > 100 ? 100 : diameter,
                                    Random::instance().uniformVSD()
                                );

    d_vsdRow = rowCol.second;

    g_log << "      Survival::cptVSDRow: S3 row = " << d_vsdRow <<
                                        " for diameter " << diameter << '\n';

    return rowCol;
}

