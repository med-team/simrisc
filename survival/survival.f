inline RowCol Survival::setVSD(double diameter)
{
    return (this->*d_cptVSDrow)(diameter);
}

inline bool Survival::metastatis() const
{
    return d_vsdRow >= 2;
}

inline VSD const &Survival::operator[](size_t idx) const
{
    return d_vsdMatrix[d_vsdRow][idx];
}

inline void Survival::vary(std::ostream &out)
{
    (this->*d_vary)(out);
}

inline void Survival::writeParameters(std::ostream &out) const
{
    (this->*d_write)(out);      // writeBreast / writeLung
}
