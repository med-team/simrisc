#define XERR
#include "survival.ih"

void Survival::lungParameters(ostream &out) const
{
    out << "Survival:\n";

    for (unsigned idx = 0, end = d_vsdMatrix.size(); idx != end; ++idx)
    {
        VSD::fmt(4, s_lungColumn[idx], 'a', 2, 8, 0, 9);
        out << d_vsdMatrix[idx] << '\n';
    }
}
