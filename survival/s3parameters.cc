//#define XERR
#include "survival.ih"

void Survival::s3parameters(ostream &out) const
{
    out <<  "S3:\n";

    for (unsigned idx = 0, end = d_s3.size(); idx != end; ++idx)
        out << "    " << d_s3[idx] << '\n';
}
