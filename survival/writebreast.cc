//#define XERR
#include "survival.ih"

void Survival::writeBreast(ostream &out) const
{
    VSD::fmt(4, "type:", 'a', 2, 8, 0, 9);
    out << "Survival:\n" << d_vsdMatrix.front() << '\n' <<
           "    bc: ";

    for (size_t idx = 0, end = d_bc.size() - 1; idx != end; ++idx)
        out << d_bc[idx] << ", ";

    if (d_bc.back() == Globals::uint16_max)
        out << "*\n";
    else
        out << d_bc.back() << '\n';
}
