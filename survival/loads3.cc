#define XERR
#include "survival.ih"

void Survival::loadS3()
{
    auto lines = Parser::any({ "prob:" });

    bool push = true;
    while (true)
    {
        LineInfo const *line = lines.get();     // load the lines of S3

        if (line == 0)                          // no (more) lines
            break;

        ProbGroup group{ SINGLE };

        if (not Parser::extract(*line, group))
            push = false;

        if (not push)
            continue;                           // continue for syntax-check
                                                // purposes

        if (group.group().nextRange(d_s3))      // add the next range
        {                                       // (0 - 11 thru 71 - *)
            d_s3.push_back(move(group));        // add the info to d_s3
            continue;
        }

        push = false;
    }
}
