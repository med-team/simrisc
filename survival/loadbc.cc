#define XERR
#include "survival.ih"

    // load the BC TNM category boundaries
void Survival::loadBC()
{
    auto lines = Parser::one({ "bc:" });

    LineInfo const *lineInfo = lines.get();

    istringstream in{ lineInfo->tail };

    char sep;
    uint16_t value;

    while (in >> sep)
    {
        if (sep == '*')
            value = Globals::uint16_max;
        else
        {
            in.unget();
            in >> value;
        }
        d_bc.push_back(value);
    }

    bool err = false;
    for (size_t idx = 1, end = d_bc.size(); idx < end; ++idx)
    {
        if (d_bc[idx - 1] >= d_bc[idx])
        {
            err = true;
            break;
        }
    }

    if (err or d_bc.back() != Globals::uint16_max)
        Err::msg(Err::NOT_CONSECUTIVE) << lineInfo->txt << endl;
}
