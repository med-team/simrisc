//#define XERR
#include "betadist.ih"

BetaDist::BetaDist(istream &in)
{
    for (size_t idx = 0; idx != size(d_param); ++idx)
        in >> d_param[idx];

    if (not in)
    {
        Err::msgTxt(Err::SPEC_ERROR);
        return;
    }

    d_xGamma = gamma_distribution<>{ d_param[2], 1 };
    d_yGamma = gamma_distribution<>{ d_param[3], 1 };
}
