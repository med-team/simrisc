//#define XERR
#include "betadist.ih"

void BetaDist::reset()
{
    d_xGamma.reset();
    d_yGamma.reset();
}
