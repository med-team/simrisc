#define XERR
#include "simulator.ih"

void Simulator::setAnalysisSource()
{
    Arg const &arg = Arg::instance();

    if (arg.option('o'))                            // -o -> specs from
    {
        d_next = true;
        d_nextSpecs = &Simulator::cmdLineAnalysis;  //       cmd line args
    }
    else
    {                                   // otherwise find the (next) Analysis:
                                        // line in the analysis file
        d_ifstream = Exception::factory<ifstream>(arg[0]);
        string line;
        while (getline(d_ifstream, line))
        {
            ++d_lineNr;
            size_t pos = line.find_first_not_of(" \t\r");
            if (pos == string::npos)
                continue;
            if (line.find("Analysis:", pos) == pos) // analysis up to EOF of
            {
                d_next = true;
                break;                              // the next analysis
            }
        }

        if (d_next == 0)
            wmsg << "No Analysis: specification found" << endl;

        d_nextSpecs = &Simulator::fileAnalysis;
    }
}
