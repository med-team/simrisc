//inline Group const &AgeGroupVSD::ageGroup() const

inline Group const &AgeGroupVSD::group() const
{
    return d_group;
}

inline uint16_t AgeGroupVSD::beginAge() const
{
    return d_group.begin();
}

inline uint16_t AgeGroupVSD::endAge() const
{
    return d_group.end();
}

inline double AgeGroupVSD::value() const
{
    return d_vsd.value();
}

inline double AgeGroupVSD::stdDev() const
{
    return d_stdDev;
}

// static
inline void fmt(unsigned valueIntWidth, unsigned valuePrec,
                unsigned distValueWdith, unsigned distPrec)
{
    VSD::fmt(valueIntWidth, valuePrec, distValueWdith, distPrec);
}

inline std::istream &operator>>(std::istream &in, AgeGroupVSD &ageGroupVSD)
{
    return ageGroupVSD.extract(in);
}

inline std::ostream &operator<<(std::ostream &out,
                                            AgeGroupVSD const &ageGroupVSD)
{
    return ageGroupVSD.insert(out);
}

inline std::ostream &operator<<(std::ostream &out,
                                AgeGroupVSDvect const &vect)
{
    return AgeGroupVSD::insert(out, vect);
}
