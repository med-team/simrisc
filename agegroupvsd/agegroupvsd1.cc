//#define XERR
#include "agegroupvsd.ih"

AgeGroupVSD::AgeGroupVSD(VaryType varyType, bool useStdDev, bool extractGroup)
:
    d_extractGroup(extractGroup),
    d_group(RANGE),
    d_vsd(varyType),
    d_stdDev(useStdDev ? 0 : -1),
    d_exp(false)
{}
