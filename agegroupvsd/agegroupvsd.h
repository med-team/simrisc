#ifndef INCLUDED_AGEGROUPVSD_
#define INCLUDED_AGEGROUPVSD_

#include <iosfwd>
#include <vector>

#include "../group/group.h"
#include "../vsd/vsd.h"

class AgeGroupVSD
{
    friend std::istream &operator>>(std::istream &in, AgeGroupVSD &group);
    friend std::ostream &operator<<(std::ostream &out,
                                    std::vector<AgeGroupVSD> const &vect);
    friend std::ostream &operator<<(std::ostream &out,
                                                AgeGroupVSD const &group);

    bool d_extractGroup;
    Group d_group;

    VSD      d_vsd;
    double   d_stdDev;      // if negative: std dev not used

    static unsigned s_indent;
    bool d_exp;

    public:
        AgeGroupVSD(VaryType varyType, bool useStdDev,
                                       bool etractGroup = true);

        Group const &group() const;                                     // .f

        uint16_t beginAge() const;                                      // .f
        uint16_t endAge() const;                                        // .f

        double value() const;                                           // .f
        double stdDev() const;                                          // .f

        void ln();

        void vary();                                                    // 1

        static void fmt(unsigned valueIntWidth, unsigned valuePrec,     // inl
                        unsigned distValueWdith, unsigned distPrec);

        static void fmt(unsigned indent,    // indentation below topLabel
                        unsigned mIntWidth, unsigned mPrec,
                        unsigned sdIntWidth, unsigned sdPrec);

                                                    // hdr should end in ':'
        static void vary(std::ostream &out, unsigned indent,            // 2
                        char const *hdr, std::vector<AgeGroupVSD> &vect);

    private:
        std::ostream &insert(std::ostream &out) const;                    // 1

        static std::ostream &insert(std::ostream &out,                    // 2
                                    std::vector<AgeGroupVSD> const &vect);

        std::istream &extract(std::istream &in);
};

using AgeGroupVSDvect = std::vector<AgeGroupVSD>;

#include "agegroupvsd.f"

#endif
