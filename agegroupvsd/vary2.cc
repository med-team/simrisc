//#define XERR
#include "agegroupvsd.ih"

                                                    // hdr should end in ':'
void AgeGroupVSD::vary(ostream &out, unsigned indent, char const *hdr,
                       AgeGroupVSDvect &vect)
{
    out <<  setw(indent) << ' ' << hdr << '\n';

    indent += 2;
    for (auto &ag: vect)
    {
        ag.vary();

        out << setw(indent) << ' ' <<
                "ageGroup:  " << ag.d_group << "    ";

        ag.d_vsd.showVary(out);
    }

    out.put('\n');
}
