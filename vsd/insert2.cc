#define XERR
#include "vsd.ih"

// static
ostream &VSD::insert(ostream &out, VSDvect const &vect)
{
    for (unsigned idx = 0, end = vect.size(); idx != end; ++idx)
    {
        out << setw(s_indent) << ' ' << s_type << ' ';

        if (s_ch == 0)
            out << idx;
        else
            out << static_cast<char>(s_ch + idx);

        out << "    " << vect[idx] << '\n';
    }
    return out;
}
