//#define XERR
#include "vsd.ih"

// static
void VSD::fmt(unsigned valueWidth, unsigned valuePrec,
              unsigned distWidth, unsigned distPrec)
{
    s_width = valueWidth + valuePrec + (valuePrec > 0);
    s_prec = valuePrec;

    Distribution::fmt(distWidth, distPrec);
}
