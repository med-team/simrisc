//#define XERR
#include "vsd.ih"

void VSD::ln()
{
    d_value = log(d_value);
    d_orgValue = d_value;
    d_exp = true;

    if (Globals::vary())
        d_dist.ln();
}
