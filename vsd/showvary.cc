//#define XERR
#include "vsd.ih"

void VSD::showVary(std::ostream &out) const  // the actually used and orig.
{
//    out << "using "  << d_value << " (configured: " << d_orgValue << ")\n";
    out << "configured: " << d_orgValue;
    if (d_value != d_orgValue)
        out << ", using: "  << d_value;
    out.put('\n');
}
