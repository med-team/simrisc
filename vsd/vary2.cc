//#define XERR
#include "vsd.ih"

// static                                       hdr should end in ':'
void VSD::vary(ostream &out, unsigned indent, char const *hdr,
                             char const *type, char ch, VSDvect &vect)
{
    out <<  setw(indent) << ' ' << hdr << '\n';
    vary(out, indent + 2, type, ch, vect);
}
