//#define XERR
#include "vsd.ih"

// static
void VSD::fmt(unsigned indent, char const *type, char ch,
              unsigned valueWidth, unsigned valuePrec,
              unsigned distWidth, unsigned distPrec)
{
    s_indent = indent;
    s_type = type;
    s_ch = ch;

    fmt(valueWidth, valuePrec, distWidth, distPrec);
}
