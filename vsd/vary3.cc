//#define XERR
#include "vsd.ih"

// static
void VSD::vary(ostream &out, unsigned indent,
                             char const *type, char ch, VSDvect &vect)
{
    for (VSD &vsd: vect)
    {
        vsd.vary();
        out << setw(indent) << ' ' << type << ": " << ch << "  ";
        vsd.showVary(out);
        ++ch;
    }

    out.put('\n');
}
