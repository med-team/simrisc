//#define XERR
#include "options.ih"

void Options::cumDeathOption(string const &value)
{
    d_specified[STARTUP] += 'd';
    d_cumDeath = value;
}
