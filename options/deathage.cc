//#define XERR
#include "options.ih"

void Options::deathAge(string const &value)
{
    d_specificAges = true;
    d_naturalDeathAge = stod(value);
}
