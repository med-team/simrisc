inline std::string const &Options::base() const
{
    return d_base[ANALYSIS];
}

inline std::string const &Options::configFile() const
{
    return d_config[ANALYSIS];
}

inline std::string const &Options::cumDeathFile() const
{
    return d_cumDeath;
}

inline size_t Options::lastCase() const
{
    return d_lastCase;
}

inline std::string const &Options::parametersFile() const
{
    return d_parametersFile[ANALYSIS];
}

inline std::string const &Options::dataFile() const
{
    return d_dataFile[ANALYSIS];
}

inline std::string const &Options::roundsFile() const
{
    return d_roundsFile[ANALYSIS];
}

inline std::string const &Options::sensitivityFile() const
{
    return d_sensitivityFile[ANALYSIS];
}

inline std::string const &Options::spreadFile() const
{
    return d_spreadFile[ANALYSIS];
}

inline bool Options::specificAges() const
{
    return d_specificAges;
}
