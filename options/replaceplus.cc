//#define XERR
#include "options.ih"

void Options::replacePlus(string &fname)
{
    if (not fname.empty() and fname.front() == '+')
        fname.replace(0, 1 + (fname[1] == '/'), d_base[ANALYSIS]);
}
