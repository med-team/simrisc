//#define XERR
#include "options.ih"

void Options::configOption()
{
    string value;
    if (d_arg.option(&value, 'C'))
        d_specified[STARTUP] += 'C';
    else
        value  = s_config;

    d_config[STARTUP] = value;

    imsg << "using configuration file `" << value << "'\n";
}
