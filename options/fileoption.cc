#define XERR
#include "options.ih"

    // dest[0] (STARTUP): as specified, dest[1] (ANALYSIS): as used
void Options::fileOption(string *dest, int option)
{
    if (not d_arg.option(&dest[STARTUP], option))
        dest[STARTUP] = s_fileName[option];   // use the default if not spec'd
    else
    {
        d_specified[STARTUP] += option;

        if (dest[STARTUP] == "!")             // specified as `!': do not use
            dest[STARTUP].clear();
        else if (option == 'P')
        {
            if (dest[STARTUP].find('/') == string::npos)
                dest[STARTUP].insert(0, 1, '+');
        }
    }
}
