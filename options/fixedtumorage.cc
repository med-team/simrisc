//#define XERR
#include "options.ih"

void Options::fixedTumorAge(double &tumorAge) const
{
    if (d_specified[ANALYSIS].find('t') != string::npos)
        tumorAge = d_tumorAge;
}
