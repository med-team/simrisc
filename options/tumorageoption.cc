//#define XERR
#include "options.ih"

void Options::tumorAgeOption(string const &value)
{
    d_specified[STARTUP] += 't';
    tumorAge(value);
}
