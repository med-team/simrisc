#define XERR
#include "options.ih"

void Options::logOption(string const &value)
{
    istringstream in{ value };

    unsigned begin;
    unsigned end;
    string fname;
    string ignored;

    if (not (in >> begin >> end >> fname))
        throw Exception{} << "--log '" << value << "': specification error";

    in >> ignored;                          // may remain empty
    g_log.set(begin, end, fname, ignored);
}
