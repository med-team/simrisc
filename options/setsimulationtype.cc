#define XERR
#include "options.ih"

void Options::setSimulationType()           // handle -c option
{
    string value;
                                                        // not specified ->
    if (not d_arg.option(&value, 'c'))                  // reconfigurable
        d_cancer[STARTUP] = Globals::defaultSimType();  // default: BREAST
    else if (Globals::findSimType(value))
    {
        d_specified[STARTUP] += 'c';                    // option -c was
        d_cancer[STARTUP] = value;                      // specified
    }
    else
        throw Exception{} << "specify '--cancer breast', "
                                     "'--cancer male', or '--cancer female'";
}
