#define XERR
#include "options.ih"

void Options::activate()
{
    // activate the options, to be used in the analyses.
    // d_specified contains the single-letter option characters
    // of the options that were specified on the command-line

    d_specified[ANALYSIS]       = d_specified[STARTUP];

    d_base[ANALYSIS]            = d_base[STARTUP];
    d_cancer[ANALYSIS]          = d_cancer[STARTUP];
    d_config[ANALYSIS]          = d_config[STARTUP];
    d_dataFile[ANALYSIS]        = d_dataFile[STARTUP];
    d_parametersFile[ANALYSIS]  = d_parametersFile[STARTUP];
    d_roundsFile[ANALYSIS]      = d_roundsFile[STARTUP];
    d_sensitivityFile[ANALYSIS] = d_sensitivityFile[STARTUP];
    d_spreadFile[ANALYSIS]      = d_spreadFile[STARTUP];
}
