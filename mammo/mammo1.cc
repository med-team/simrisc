#define XERR
#include "mammo.ih"

//  Mammo:
//      costs:          64
//
//      #      bi-rad:  a       b       c       d
//      dose:           3       3       3       3

//  #Mammo:
//      m:             .136    .136    .136    .136 
//
//      #             ageGroup
//      specificity:  0 - 40:  .961     40 - *: .965
//
//      #       1       2       3        4
//  #Mammo:
//      beta:  -4.38    .49     -1.34    -7.18
//
//  #Mammo:
//      systematicError:  0.1

Mammo::Mammo(Tumor const &tumor)
:
    ModBase("Mammo"),
    d_tumor(tumor)
{
    if (not defined())
        return;

    setSystematicError();                   // must be before doseBase
    setM();
    setBeta();

    doseBase(d_dose);                       // ModBase members
    specificityBase(d_specVect);
}
