//#define XERR
#include "mammo.ih"

void Mammo::vVary(ostream &out)
{
    out << "  Mammo:\n";

    VSD::vary(out, 4, "Dose:", "bi-rad", 'a', d_dose);
    VSD::vary(out, 4, "M:"   , "bi-rad", 'a', d_m);
    VSD::vary(out, 4, "Beta:", "nr", '1', d_beta);

    AgeGroupVSD::vary(out, 4, "Specificity:", d_specVect);
}
