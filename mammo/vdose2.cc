//#define XERR
#include "mammo.ih"

double Mammo::vDose(uint16_t idx) const
{
    return d_dose[idx].value();
}
