#ifndef INCLUDED_MAMMO_
#define INCLUDED_MAMMO_

#include "../enums/enums.h"
#include "../modbase/modbase.h"
#include "../agegroupvsd/agegroupvsd.h"

//  Mammo:
//      costs:          64
//
//      #      bi-rad:  a       b       c       d
//      dose:           3       3       3       3
//      m:             .136    .136    .136    .136
//
//      #             ageGroup
//      specificity:  0 - 40:  .961     40 - *: .965
//
//      #       1       2       3        4
//      beta:  -4.38    .49     -1.34    -7.18
//
//      systematicError:  0.1

class Tumor;

class Mammo: public ModBase
{
    Tumor const &d_tumor;

    double d_sysErr;

    VSDvect d_dose;
    VSDvect d_m;
    VSDvect d_beta;

    AgeGroupVSDvect d_specVect;

    public:
        Mammo(Tumor const &tumor);
        ~Mammo() override;

    private:
        void setSystematicError();

        void setBeta();
        void setM();

        VSDvect const *vDose() const override;              // 1
        double vDose(uint16_t) const override;              // 2
        void vInsert(std::ostream &out) const override;
        double vSensitivity(size_t idx) const override;
        double vSpecificity(double age) const override;
        void vVary(std::ostream &out) override;

};

//Modalities:
//
//  Mammo:
//      costs:          64
//
//      #      bi-rad:  a       b       c       d
//      dose:           3       3       3       3
//      m:             .136    .136    .136    .136
//
//      #             ageGroup
//      specificity:  0 - 40:  .961     40 - *: .965
//
//      #       1       2       3        4
//      beta:  -4.38    .49     -1.34    -7.18
//
//      systematicError:  0.1



#endif
