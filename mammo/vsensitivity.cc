//#define XERR
#include "mammo.ih"

//code
// cf. Isheden, G., & Humphreys, K. (2017). Modelling breast cancer tumour
// growth for a stable disease population. Statistical Methods in Medical
// Research, 28(3), 681-702.

double Mammo::vSensitivity(size_t idx) const
{
    double diameter = d_tumor.diameter();
    double mValue = d_m[idx].value();

    double expBeta =
        exp(
            d_beta[0].value()            +
            d_beta[1].value() * diameter +
            d_beta[2].value() * mValue   +
            d_beta[3].value() * mValue / (diameter * diameter)
        );

    double ret = (1 - d_sysErr) * expBeta / (1 + expBeta);

    g_log << __FILE__ " idx = " << idx << ", diameter: " << diameter <<
            ", m: " << mValue << ", sens.: " << ret << "\n";

    return ret;
}
//=

