//#define XERR
#include "distribution.ih"

// static
DistType Distribution::xlat(LineInfo const &info, string const &name)
{
    auto ret = find(name);

    if (ret == N_DISTRIBUTIONS)
        Err::msg(Err::UNDEFINED_DIST) << '`' << name << '\'' << endl;

    return ret;
}
