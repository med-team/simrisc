//#define XERR
#include "distribution.ih"

Distribution::Distribution(VaryType varyType)
:
    d_value(Globals::vary() ? s_varyParam[varyType] : &Distribution::accept),
    d_exp(false)
{}
