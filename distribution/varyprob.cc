#define XERR
#include "distribution.ih"

double Distribution::varyProb(double orgValue) const
{
    for (unsigned count = 0; count != MAX_VARY_TRIES; ++count)
    {
        if (
            double ret = varyMean(orgValue);
            Globals::proportion(ret)
        )
            return ret;
    }

    throw Exception{} <<
        "failed to obtain spread proportion for " <<
        orgValue << " (spread parameter = " << d_confValue <<
                    ", distribution: " << name(d_distType) << ')';
}
