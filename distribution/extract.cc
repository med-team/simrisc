#define XERR
#include "distribution.ih"

    // So far the extraction has extracted the mean. Next comes the stddev and
    // dist, but stddev may not be present -> dist is not present
    //     #               mean    stddev  dist
    //     specificity:    99.2    .076    Normal
    //
    // for Beta destributions, used with LC, the name and parameters of the
    // Beta distribution are provided, which are used when parameter spread
    // is configured
    //
    // # LC:    eta   beta     dist    constant factor   aParam    bParam
    // male:    -1.4   .32     Beta    .234091  1.72727  2.664237  5.184883
    // female:  -1.4  1.40     Beta    .744828   .818966 3.366115  4.813548

    // the Distribution is extracted if d_confValue is present.
    // if present it must be >= 0

istream &Distribution::extract(istream &in)
{
    if (not Globals::vary())        // no parameter variation -> no
        return in;                  // distribution extraction

    string next;                    // inspect the next field on 'in'

                                    // try extracting the SD parameter
    if (not (in >> next))           // nothing is extracted, so this is not
    {                               // a Beta dist. spec, or a dist. w/o SD
        d_distType = NORMAL_VARY;
        prepareVary(in);            // in: unused here
        in.clear();
        return in;
    }

    if (next == "Beta")             // this is a Beta distribution
    {
        d_distType = BETA_VARY;
        Random::instance().prepareBeta(in);
        d_value = &Distribution::varyBeta;
        return in;
    }

    d_confValue = stod(next);       // no beta, so it must be a value
    if (d_confValue < 0)
    {
        Err::msgTxt(Err::NEGATIVE);
        in.fail();
        return in;
    }
                // value was extracted: the distribtion name must be next
    if (in >> next and (d_distType = find(next)) != N_DISTRIBUTIONS)
    {
        prepareVary(in);
        return in;
    }

    Err::msgTxt(Err::UNDEFINED_DIST);
    in.fail();

    return in;
}
