// static
inline std::string const &Distribution::name(DistType dist)
{
    return s_name[dist];
}

inline std::istream &operator>>(std::istream &in, Distribution &dist)
{
    return dist.extract(in);
}

inline std::ostream &operator<<(std::ostream &out, Distribution const &dist)
{
    return dist.insert(out);
}

inline DistType Distribution::type() const
{
    return d_distType;
}

inline double Distribution::value(double orgValue) const
{
    return (this->*d_value)(orgValue);
}
