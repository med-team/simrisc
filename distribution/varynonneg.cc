#define XERR
#include "distribution.ih"

double Distribution::varyNonNeg(double orgValue) const
{
    for (unsigned count = 0; count != MAX_VARY_TRIES; ++count)
    {
                                    // try to obtain a valid spreaded SD
        if (double ret = varyMean(orgValue); ret >= 0)
            return ret;
    }

    throw Exception{} <<
        "failed to obtain non-negative spread value for " <<
        orgValue << " (spread = " << d_confValue <<
                    ", distribution: " << name(d_distType) << ')';
}
