//#define XERR
#include "distribution.ih"

StringVect Distribution::s_name                     // see enums.h
{
    "Uniform"    ,      // UNIFORM
    "Uniform"    ,      // UNIFORM_CASE
    "LogNormal"  ,      // LOGNORMAL

    "Normal",           // NORMAL_VARY
    "Uniform"    ,      // UNIFORM_VARY
    "LogNormal"  ,      // LOGNORMAL_VARY

    "Beta"       ,      // BETA_VARY
};

    // the beta distribution have been initialized for male/female simulations
    // so only one varyBeta can be used.
double (Distribution::*Distribution::s_varyParam[])(double orgValue) const =
{
    &Distribution::varyMean,    // VARY_MEAN,   all OK
    &Distribution::varyNonNeg,  // VARY_NONNEG, values must be >= 0
    &Distribution::varyProb,    // VARY_PROB,   values must be probabilities
    &Distribution::varyBeta,    // VARY_BETA,      the Beta distributions
};

unsigned Distribution::s_width = 0;
unsigned Distribution::s_precision = 0;
