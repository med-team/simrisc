#define XERR
#include "distribution.ih"

double Distribution::varyBeta(double orgValue) const
{
    return Random::instance().betaVary();
}
