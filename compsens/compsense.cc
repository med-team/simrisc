#include <fstream>
#include <iostream>
#include <string>

using namespace std;

int main()
{
    ifstream in{"../org/simrisc/test3/sensitivity.txt"};

    in.ignore(1000, '\n');
    double orgYears;
    double orgCosts;

    in >> orgYears >> orgYears >> orgCosts;

    cout << "Originally:\n" << orgYears << ' ' << orgCosts << '\n';

    in.close();
    in.open("../output/sensitivity.txt");

    in.ignore(1000, '\n');
    in.ignore(1000, '\n');
    in.ignore(1000, '\n');

    double simYears;
    double simCosts;
    char comma;

    in >> simYears >> comma >> simYears >> comma >> simCosts;

    cout << "New simrisc:\n" << simYears << ' ' << simCosts << '\n';

    double q1 = orgYears / simYears;
    double q2 = orgCosts / simCosts;

    if (q1 < 1)
        q1 = 1 / q1;

    if (q2 < 1)
        q2 = 1 / q2;

    cout << q1 << "     " << q2 << '\n';
}
