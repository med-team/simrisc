//#define XERR
#include "modalities.ih"

uint16_t Modalities::find(std::string const &modality) const
{
    auto iter = std::find_if(d_modalityIDs.begin(), d_modalityIDs.end(),
                        [&](string const &available)
                        {
                            return modality == available;
                        }
                );

    return iter != d_modalityIDs.end() ?
                        iter - d_modalityIDs.begin()
                    :
                        UNDEFINED;
}
