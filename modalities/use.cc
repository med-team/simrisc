//#define XERR
#include "modalities.ih"

vector<ModBase *> Modalities::use(Uint16Vect const &active) const
{
    vector<ModBase *>ret;

    for (uint16_t idx: active)
        ret.push_back(d_modBaseVect[idx]);      // store the Modality ptr
                                                // return ptrs to the
    return ret;                                 // requested Modality handlers
}
