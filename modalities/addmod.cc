//#define XERR
#include "modalities.ih"

void Modalities::addMod(ModBase *modBase)   // d_base in ModBase, initialized
{                                           // to { modName, "costs:" },
                                            // read by costs/costs1.cc
    if (not modBase->defined())
    {
        delete modBase;
        return;
    }

    d_modBaseVect.push_back(modBase);
    d_modalityIDs.push_back(modBase->id());
}
