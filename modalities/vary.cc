//#define XERR
#include "modalities.ih"

void Modalities::vary(ostream &out)
{
    out << "Modalities\n";

    for (ModBase *modBase: d_modBaseVect)
        modBase->vary(out);

    out.put('\n');
}
