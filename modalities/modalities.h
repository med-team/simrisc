#ifndef INCLUDED_MODALITIES_
#define INCLUDED_MODALITIES_

// See README

#include <iosfwd>
#include <vector>

#include "../typedefs/typedefs.h"
#include "../enums/enums.h"

class ModBase;
class Tumor;

namespace FBB
{
    class CSVTable;
}

class Modalities
{
    Tumor const &d_tumor;               // used by Mammo and CT


    std::vector<ModBase *> d_modBaseVect;   // defined modalities
    StringVect d_modalityIDs;               // and their IDs
    StringSet d_active;                     // the active modalities
    std::vector<ModBase *> d_activeVect;    // ptrs to active modalities
    
    public:
        Modalities(Tumor const &tumor);
        ~Modalities();

                            // visit all modalities and initialize
                            // their data, see loop/initialize
            // see also Loop::d_nModalityFP: modality specific, to become
            //      a size_t variables in each used modality
        void resetCounters(size_t nRounds);

                                // returns UNDEFINED if 'modality' is unknown
        uint16_t find(std::string const &modality) const;

            // return a vector of ModBase ptrs matching the names specified
            //  in 'active'
        std::vector<ModBase *> use(Uint16Vect const &active) const;
        StringVect const &modalityIDs() const;

        void vary(std::ostream &out);

        void writeParameters(std::ostream &out) const;

                                                        // determine the Mod. 
        size_t roundFmt(FBB::CSVTable &tab) const;      // #fields & width   

        void roundHeaders(FBB::CSVTable &tab) const;    // write the Mod names
                                                        // write the Mod data
        void writeRounds(FBB::CSVTable &tmp, size_t round) const;

        void active();                          // sets d_activeVect
        void activate(std::string const &modality);       

    private:
        void addMod(ModBase *modBase);
};

// double specificity(age) const: age may not be negative
// currently not used
// double const *hasDose(StringVect const &ids) const;

#include "modalities.f"

#endif
