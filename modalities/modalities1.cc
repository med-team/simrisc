#define XERR
#include "modalities.ih"

#include "../parser/parser.h"

Modalities::Modalities(Tumor const &tumor)
:
    d_tumor(tumor)
{
    if (not Globals::isBreast())
        addMod(new CT{ d_tumor });
    else
    {
        addMod(new Mammo{ d_tumor });
        addMod(new Tomo);
        addMod(new MRI);
    }
}
