#define XERR
#include "modalities.ih"

size_t Modalities::roundFmt(CSVTable &tab) const
{
    if (d_activeVect.size() == 1)
    {
        tab.fmt(tab.size()) << "number of";
        return 1;
    }

    size_t nActive = d_activeVect.size();
    for (size_t idx = 0; idx != nActive; ++idx)
        tab.fmt(tab.size()) << "12345";

    return nActive;
}

