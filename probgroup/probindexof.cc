//#define XERR
#include "probgroup.ih"

// see README.probgroup

// with Survival::cptVSDrow 'value' is the tumor diameter
// then, the group is determined from the diameter, and in the selected
// group the column index of that row of S3 is omputed as 'idx'

// static
RowCol ProbGroup::probIndexOf(Vector const &vect,
                                         double value, double prob)
{
    size_t rowIdx = 0;

    for (ProbGroup const &group: vect)
    {
        if (group.contains(value))
        {
            uint16_t idx = group.indexOf(prob);
            return RowCol{ rowIdx, idx };
        }
        ++rowIdx;
    }

    return RowCol{ static_cast<uint16_t>(string::npos), 0 };
}
