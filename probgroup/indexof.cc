//#define XERR
#include "probgroup.ih"

    // 0 <= prob <= 1 and so indexOf always returns a column

extern size_t g_caseIdx;
extern size_t g_err;

uint16_t ProbGroup::indexOf(double prob) const
{
//if (g_caseIdx >= g_err)// and prob > .9999)
//{
//xerr("PROBGROUP: " << g_caseIdx << " prob: " << prob << '\n');
//for (double prob: d_cumProbs)
//cerr << prob << ' ';
//cerr << '\n';
//}

    uint16_t ret =
        prob > d_cumProbs.back() ?
                d_cumProbs.size() - 1
            :
                find_if(d_cumProbs.begin(), d_cumProbs.end(),
                    [&](double cumProb)
                    {
                        return prob <= cumProb;
                    }
                ) - d_cumProbs.begin();

    return ret;
}
