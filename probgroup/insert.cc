//#define XERR
#include "probgroup.ih"

std::ostream &ProbGroup::insert(ostream &out) const
{
    out << s_labels[d_breastCancer][0] << d_group <<
                                          s_labels[d_breastCancer][1];
    if (not d_breastCancer)
        Globals::setWidthPrec(out, 2, 4);

    for (size_t idx = 0; idx != N_PROBCATS; ++idx)
        out << static_cast<char>('a' + idx) << ": " <<
                setw(4) << d_prob[idx] << ", ";

    return out;
}
