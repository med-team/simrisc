#define XERR
#include "tableparams.ih"

void TableParams::checkTable()
try
{
    if (d_error)
        return;

    if (d_riskTable.size() < 2)     // at least 2 pairs must be specified
        throw "at least 2 pairs must be specified";

    if (d_riskTable.front().first != 0) // ensure a first {0, ..} is available
        d_riskTable.insert(d_riskTable.begin(), DoublePair{0, 0});

    checkIncreasing();
                                                // if no MAX_AGE at the end:
    if (d_riskTable.back().first < MAX_AGE)     // add a final end-age value
    {
        d_riskTable.push_back(
                                DoublePair{ MAX_AGE, 
                                    linear(MAX_AGE, 
                                            *(d_riskTable.rbegin() + 1),
                                            d_riskTable.back() )
                                }
                            );
                                            // ensure the last value is <= 1
        if (DoublePair &last = d_riskTable.back(); last.second > 1.0)
            last.second = 1;
    }
}
catch (char const *txt)
{
    d_error = true;
    Err::msg(Err::TABLEPARAMS) << txt << '\n';
    return;
}
