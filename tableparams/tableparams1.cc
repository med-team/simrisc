#define XERR
#include "tableparams.ih"

TableParams::TableParams(StringVect &base, size_t idx, bool setProb)
:
    Params(base, idx, setProb),
    d_error(false)
{
    if (prob() < 0)         // invalid probability setting
        return;

    fillRiskTable(base);

    
    
}
