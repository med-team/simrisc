#ifndef INCLUDED_TABLEPARAMS_
#define INCLUDED_TABLEPARAMS_

// the Parser has already confirmed that 'riskTable:' was used
// (cf. incidence/setincidence2.cc)


#include "../params/params.h"
#include "../typedefs/typedefs.h"

class TableParams: public Params
{
    bool d_error;
    Double2Vect d_riskTable;        // first: age, second: cumprob for age

    public:
        TableParams(StringVect &base, size_t idx, bool setProb);

    private:
        void fillRiskTable(StringVect &base);
        bool appendPair(std::string const &line);
        void checkTable();
        void checkIncreasing();
        static double linear(double age, 
                        DoublePair const &from, DoublePair const &to);

        void v_cptTumorRisk(DoubleVect &ageValues) override;
        void v_vary(std::ostream &out) override;
        void v_writeParameters(std::ostream &out) const;

};
        
#endif



