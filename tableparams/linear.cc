#define XERR
#include "tableparams.ih"

//linear interpolation: for a = d_riskTable[idx].first
//                          p = d_riskTable[idx].second
//                          b = d_riskTable[idx + 1].first
//                          q = d_riskTable[idx + 1].second
//                          x = age, a is searched for a <= x
//                       compute f(x), where a <= x <= b as
//                       p + (q - p) / b - a) * (x - a)
//                    =  p + (q - p) * (x - a) / b - a)

// static
double TableParams::linear(double age, 
                           DoublePair const &from, DoublePair const &to)
{
    return from.second + (to.second - from.second) * (age - from.first) / 
                         (to.first - from.first);
}
