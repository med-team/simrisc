//#define XERR
#include "params.ih"

StringVect const Params::s_label
{
    Globals::label(BREAST),
    "BRCA1:",
    "BRCA2:"
};
