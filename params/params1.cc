#define XERR
#include "params.ih"

Params::Params(StringVect &base, size_t idx, bool setProb)
:
    d_idx(idx)
{
    if (setProb)
    {
        base.back() = "probability:";
        if (not Parser::proportion(base, d_prob))
            d_prob = -1;                            // prob-setting failed
    }
}
