#define XERR
#include "params.ih"

void Params::writeParameters(std::ostream &out)
{
    Globals::setPrecision(out, 1) <<
        setw(6) << ' ' << label() << '\n' <<
           setw(8) << ' ' << "probability:   " <<
                prob() <<
                (
                    Globals::isZero(prob()) ?
                        "     (carrier ignored)\n"
                    :
                        "\n"
                );

    v_writeParameters(out);
}

