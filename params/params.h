#ifndef INCLUDED_PARAMS_
#define INCLUDED_PARAMS_

// Params is the base class for VSDParams and for TableParams.
//
// VSDParams handles the computation of the tumor risk values based on 
//      lifetimeRisk, meanAge, and stdDev
//
// TableParams handles the computation of the tumor risk values based on the
//      riskTable values
//
// The base class defines member 
//      virtual void cptTumorRisk(DoubleVect &ageValues)
// which calls the overriding member from the derived class.
//
// The vector d_params contains unique_ptr<Params>, so for each defined
// Incidence type the appropriate ageValues is computed

#include "../typedefs/typedefs.h"

class Params
{
    unsigned d_idx;                 // see setIncidence (.2): this is the
                                    // index used in the d_params vector

                                    // set to -1 if prob. setting fails in 
    double   d_prob = 1;            // the constructor

    static StringVect const s_label;    // labels of the BC incidence modes

    public:
        Params(StringVect &base, size_t idx, bool setProb);
        virtual ~Params();

        void cptTumorRisk(DoubleVect &ageValues);
        double   prob() const;                                      // .f
        void vary(std::ostream &out);                               // .f
        void writeParameters(std::ostream &out);

        static std::string const &label(size_t idx);                // .f
        static size_t nLabels();                                    // .f

    protected:
        void invalid();     // sets d_prob = -1                     // .f
        std::string const &label() const;                           // .f

    private:
        void virtual v_cptTumorRisk(DoubleVect &ageValues) = 0;
        void virtual v_vary(std::ostream &out) = 0;
        void virtual v_writeParameters(std::ostream &out) const = 0;
};

#include "params.f"
        
#endif
