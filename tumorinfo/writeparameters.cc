//#define XERR
#include "tumorinfo.ih"

void TumorInfo::writeParameters(ostream &out) const
{
    out << "Tumor:\n";

    d_beir7.writeParameters(out);
    d_incidence.writeParameters(out);
    d_growth.writeParameters(out);
    d_survival.writeParameters(out);

}
