#ifndef INCLUDED_TUMORINFO_
#define INCLUDED_TUMORINFO_

#include <iosfwd>

#include "../globals/globals.h"

#include "../incidence/incidence.h"
#include "../beir7/beir7.h"
#include "../growth/growth.h"
#include "../survival/survival.h"

class Scenario;

class TumorInfo: public Globals
{
    Incidence d_incidence;
    Beir7     d_beir7;                  // initialized by Beir7's constructor
    Growth    d_growth;
    Survival  d_survival;

    DoubleVect d_cumRisk;

    public:
        void vary(std::ostream &out);           // vary parameters

//        Incidence const &incidence() const;

        uint16_t  induction() const;            // beir model to use
        Beir7     const &beir7() const;
        Growth    const &growth() const;
        Survival  &survival();

        void cumTotalRisk(DoubleVect const &radiationRisk);

        DoubleVect const &cumTotalRisk() const;

        void writeParameters(std::ostream &out) const;

    private:
        void totalRiskFile() const;
};

//inline Incidence const &TumorInfo::incidence() const
//{
//    return d_incidence;
//}

inline Beir7 const &TumorInfo::beir7() const
{
    return d_beir7;
}

inline Growth const &TumorInfo::growth() const
{
    return d_growth;
}

inline Survival &TumorInfo::survival()
{
    return d_survival;
}

inline DoubleVect const &TumorInfo::cumTotalRisk() const
{
    return d_cumRisk;
}

#endif
