//#define XERR
#include "tumorinfo.ih"

void TumorInfo::vary(ostream &out)
{
    out << "Tumor:\n";

    d_beir7.vary(out);
    out.put('\n');

    d_growth.vary(out);
    d_incidence.vary(out);
    d_survival.vary(out);
}
