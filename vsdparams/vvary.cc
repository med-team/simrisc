//#define XERR
#include "vsdparams.ih"

// overrides
void VSDParams::v_vary(ostream &out)
{
    for (auto &vsd: d_vsd)
        vsd.vary();

    out << "    " <<
                (
                    not Globals::isBreast() ?
                        Globals::simTypeLabel(LC)
                    :
                        label()
                ) << "\n"
           "      lifetimeRisk:   ";

    d_vsd[RISK].showVary(out);

    out << "      meanAge:        ";
    d_vsd[MEAN].showVary(out);
}
