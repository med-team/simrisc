#ifndef INCLUDED_VSDPARAMS_
#define INCLUDED_VSDPARAMS_

#include "../params/params.h"
#include "../vsd/vsd.h"

class VSDParams: public Params
{
    enum Indices
    {
        RISK,   // lifetimeRisk
        MEAN,
        SDEV
    };

    VSD     d_vsd[3] = { VSD{ VARY_PROB },          // lifetimeRisk
                         VSD{ VARY_NONNEG },        // meanAge
                         VSD{ VARY_NONNEG } };      // stdDev
    public:
        VSDParams(StringVect &base, size_t idx, bool setProb);

        ~VSDParams() override;

    private:
        void v_cptTumorRisk(DoubleVect &ageValues) override;
        void v_vary(std::ostream &out) override;
        void v_writeParameters(std::ostream &out) const;
};

#endif
