#define XERR
#include "vsdparams.ih"

VSDParams::VSDParams(StringVect &base, size_t idx, bool setProb)
:
    Params(base, idx, setProb)
{
    if (prob() < 0)             // invalid probability setting
        return;

    base.back() = "lifetimeRisk:";
    bool ok = Parser::one(base, d_vsd[RISK]);

    base.back() = "meanAge:";
    ok = Parser::one(base, d_vsd[MEAN]) and ok;

    base.back() = "stdDev:";
    ok = Parser::one(base, d_vsd[SDEV]) and ok;

    if (not ok)
        invalid();                  // error in the param. specs: prob = -1
        
}
