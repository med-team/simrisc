#define XERR
#include "analysis.ih"

void Analysis::actualize(Parser::OptionsVect const &optionsVect)
{
    auto &options = Options::instance();

    options.activate();                         // activate the startup
                                                // (default/command-line)
                                                // options

    for (auto const &spec: optionsVect)         // visit all options specified
    {                                           // in this analysis

                                                // visit all long options
        for (auto const *ptr = g_longOpts; ptr != g_longEnd; ++ptr)
        {
            string const &name = ptr->longName();

            if (name == spec.name)              // found an option to alter
            {
                                                // try to change it
                if (not options.alter(ptr->optionChar(), spec.value))
                    wmsg << "ignored analysis option specification " <<
                            spec.name << " (line " << spec.lineNr << ')' <<
                            endl;
                break;                          // done with this option
            }
        }
    }

    options.actualize();                        // obtain the actual options
}
