#!/usr/bin/make -f

DH_VERBOSE := 1
export LC_ALL=C.UTF-8

include /usr/share/dpkg/default.mk
# this provides:
# DEB_SOURCE: the source package name
# DEB_VERSION: the full version of the package (epoch + upstream vers. + revision)
# DEB_VERSION_EPOCH_UPSTREAM: the package's version without the Debian revision
# DEB_VERSION_UPSTREAM_REVISION: the package's version without the Debian epoch
# DEB_VERSION_UPSTREAM: the package's upstream version
# DEB_DISTRIBUTION: the distribution(s) listed in the current entry of debian/changelog
# SOURCE_DATE_EPOCH: the source release date as seconds since the epoch, as
#                    specified by <https://reproducible-builds.org/specs/source-date-epoch/>

# For reproducible builds of documentation.
# Also see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=950603
export FORCE_SOURCE_DATE=1

export DEB_BUILD_HARDENING = 1
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

export ICMAKE_CPPSTD = --std=c++26

export CPPFLAGS = $(shell dpkg-buildflags --get CXXFLAGS)
export CXXFLAGS += ${ICMAKE_CPPSTD}  -Wall -D_FORTIFY_SOURCE=2

# export ICMAKE_CPPSTD = $(shell dpkg-buildflags --get CPPSTD)
# export ICMAKE_CPPSTD += --std=c++26 -Wall -Werror -D_FORTIFY_SOURCE=2

export LDFLAGS = $(shell dpkg-buildflags --get LDFLAGS) -Wl,-z,now

export CXX = g++

%:
	dh $@

override_dh_auto_clean:
	./build clean || true
	rm -rf tmp/
	dh_auto_clean

override_dh_auto_build-arch:
	# compile the package.
	./build program
	./build man
	dh_auto_build -a

override_dh_auto_install-arch:
	# Add here commands to install the package into debian/simrisc.
	./build install bdm debian/simrisc
	dh_installdocs hierarchy.odp -a
	dh_auto_install -a
