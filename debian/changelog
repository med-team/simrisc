simrisc (16.02.00-1) unstable; urgency=medium

  * New upstream version
    - Natural deaths occurring before tumor onsets are no longer using
      'UNDETECTED_XXX' exit codes but 'NATURAL_XXX' exit codes.
  * debian/tests/expected* files were updated.
  * debian/rules updated the used C++ standard version to c++26.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sun, 01 Sep 2024 11:24:30 +0200

simrisc (16.01.00-1) unstable; urgency=medium

  * New upstream version
    - Added parameter e to the lung-cancer simulation, and updated the
      simriscparams(7) man-page accordingly.
    - Reorganized simrisc's to avoid multiply implemented elements
      deriving from versions before simrisc 12.00.00.
    - Simplified organization of the configuration file and Analysis
      specifications
    - Tumor incidence parameters can also be specified using a table, in
      addition to using distribution parameters.
    - Man-pages updated.
  * Removed superfluous definitions from the debian/rules file
  * Updated debian/debian/tests/*expected.gz.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Wed, 05 Jun 2024 10:09:02 +0200

simrisc (15.05.00-1) unstable; urgency=medium

  [ Frank B. Brokken ]
  * New upstream release repairs several inconsistencies. See the upstream
    changelog for details.
  * Icmake >= 11.01.02 is required for package construction.

  [ tony mancill ]
  * Update debian/test expected files to match output from 15.05.00

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Mon, 29 Jan 2024 13:19:08 +0100

simrisc (15.04.00-1) unstable; urgency=medium

  * New upstream release adds the cumulative death-proportions (when used) to
    the params.txt file.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 09 Dec 2023 13:10:17 +0100

simrisc (15.03.01-2) UNRELEASED; urgency=medium

  * Update d/rules to clean up fully after build (Closes: #1048227)
  * Edit changelog entry for 15.03.01-1; there was no change to copyright.

 -- tony mancill <tmancill@debian.org>  Wed, 01 Nov 2023 15:34:31 -0700

simrisc (15.03.01-1) unstable; urgency=medium

  [ Frank B. Brokken ]
  * New upstream version checks for incorrectly specified lines when the
    cumulative death proportions are read.
  * Bumped icmake's version in debian/control to 10.06.00
    (for ICMAKE_CXXFLAGS)
  * Updated debian/rules: it now specifies ICMAKE_CXXFLAGS

  [ Andreas Tille ]
  * cme fix dpkg-control
  * Pass clean target even if ./build does not exist

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 28 Oct 2023 15:42:16 +0200

simrisc (15.03.00-2) unstable; urgency=medium

  * Add new autopkgtest test case to simrisc

 -- tony mancill <tmancill@debian.org>  Sun, 02 Jul 2023 10:37:44 -0700

simrisc (15.03.00-1) unstable; urgency=medium

  * Upstream version 15.03.00 fixes a bug in an index computation
    (intensively) used with lung cancer simulations.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 01 Jul 2023 17:15:27 +0200

simrisc (15.02.00-1) unstable; urgency=medium

  [ Frank B. Brokken ]
  * New upstream version 15.02.00 uses wider variable types for storing
    treatment costs. Previously 16 bit values, now 64 bit values.
  * Compilation uses the --std=c++23 option.

  [ tony mancill ]
  * Update autopkgtest for simrisc 15.02.00 (longer output lines)

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Fri, 16 Jun 2023 10:03:01 +0200

simrisc (15.01.00-2) unstable; urgency=medium

  * Upload to 15.01.00 to unstable

 -- tony mancill <tmancill@debian.org>  Sun, 11 Jun 2023 09:55:56 -0700

simrisc (15.01.00-1) experimental; urgency=medium

  * New upstream version 15.01.00 adds new options --log and --tnm, adds TNM
    categories to the generated data, and defines TNM tumor diameter
    boundaries for breast-cancer simulations in the standard simrisc
    configuration file. The specifications of the lung cancer tumor diameter
    boundaries were slightly altered and specify the (inclusive) upper limits
    of the tumor diameters instead of from-to ranges. The man-pages were
    updated accordingly.

  * Updated the copyright years in the debian/copyright file

  * Updated the Debian Standards version in debian/control

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Mon, 08 May 2023 10:40:55 +0200

simrisc (15.00.00-1) unstable; urgency=medium

  [ Frank B. Brokken ]
  * New upstream version 15.00.00
  * New major version: simrisc can now also be used to perform lung cancer
    simulations. See the simrisc(1) and simriscparams(7) manpages and the
    upstream changelog.
  * Updated the debian/rules file
  * Add debian/gbp.conf for DEP-14

  [ tony mancill ]
  * Update autopkgtest simrisc.expected for upstream 15.00.00

 -- tony mancill <tmancill@debian.org>  Sun, 11 Dec 2022 18:12:02 -0800

simrisc (14.05.01-1) unstable; urgency=medium

  * New upstream version 14.05.01
  * debian/rules sources the C++ version from the upstream 'c++std' file
  * Drop the GCC-12 patch incorporated upstream.  Thank you Nilesh.
  * Specified Standards-Version: 4.6.1 in 'debian/control'

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 17 Sep 2022 17:01:14 +0200

simrisc (14.05.00-4) unstable; urgency=medium

  * Team upload.
  * Add patch to fix gcc12 FTBFS (Closes: #1013037)

 -- Nilesh Patra <nilesh@debian.org>  Mon, 25 Jul 2022 15:26:18 +0530

simrisc (14.05.00-3) unstable; urgency=medium

  * Update autopkgtest for new output format in upstream 14.05.00

 -- tony mancill <tmancill@debian.org>  Thu, 09 Sep 2021 21:18:42 -0700

simrisc (14.05.00-2) unstable; urgency=medium

  * Upload to unstable
  * Bump Standards-Version to 4.6.0

 -- tony mancill <tmancill@debian.org>  Thu, 09 Sep 2021 20:27:56 -0700

simrisc (14.05.00-1~exp0) experimental; urgency=medium

  [ Frank B. Brokken ]
  * Upstream added option --err (-e) to select the previously used but
    inorrect algorithm for computing the Beir7 risk vector, instead of using
    the (now default) correct algorithm for computing the risk vector.

  [ Nilesh Patra ]
  * d/p/make-reproducible.patch: Use SOURCE_DATE_EPOCH
    if available to make build reproducible (Closes: #985160)
  * Append relevant options to fix blhc

  [ tony mancill ]
  * Upload to experimental
  * Remove debian/patches/cppflags.patch; applied upstream
  * Description synopsis should not be a sentence (lintian warning)
  * Add Bug-Submit and Repository fields to DEP-12 metadata

 -- tony mancill <tmancill@debian.org>  Thu, 27 May 2021 20:55:19 -0700

simrisc (14.04.00-1) UNRELEASED; urgency=medium

  * New upstream version 14.04.00 supports varying parameter values when
    'spread: true' is specified for all modalities.
  * Original and actually used parameter values are written to (by default)
    spread-$.txt, where $ is replaced by the loop's iteration index.
  * The data file containing the case-data now has an 18th column covering for
    each simulated case which screening rounds were attended and what
    diagnostic results were obtained.
  * Man-pages were updated
  * Nilesh's reprodicible patch is handled by providing a fixed date in
    documentation/manual/simrisc.yo, automatically updated upstream at new
    releases.
  * Nilesh's addition of the FORTIFIY_SOURCE parameter to gcc's preprocessing
    run is not required as it doesn't involve compilation: the patch was
    removed.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sun, 11 Apr 2021 12:09:35 +0200

simrisc (14.03.00-1~exp0) experimental; urgency=medium

  [ Frank B. Brokken ]
  * New upstream version 14.03.00
    Adds the specification 'round: none' to suppress using screening
    round ages.

  [ Nilesh Patra ]
  * Add salsa-ci.yml

  [ tony mancill ]
  * Freshen debian/copyright
  * Set FORCE_SOURCE_DATE=1 so texlive respects SOURCE_DATE_EPOCH for
    reproducible builds
  * Drop unneeded dependency on texlive-fonts-recommended
  * Add debian/gbp.conf for debian/experimental branch
  * Add autopkgtest case

 -- tony mancill <tmancill@debian.org>  Sun, 28 Feb 2021 16:40:09 -0800

simrisc (14.02.00-1) unstable; urgency=medium

  * New upstream release updated the man-pages and requires the use of
    'Analysis:' instead of 'analysis:' in analysis files
  * Default option values are reset at each Analysis: specification
  * Option  --cases was renamed to --last-case

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Wed, 27 Jan 2021 16:40:35 +0100

simrisc (14.01.00-1) unstable; urgency=medium

  * New upstream release allows specification of m-parameters for each
    bi-rad category.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Fri, 22 Jan 2021 11:20:23 +0100

simrisc (14.00.00-1) unstable; urgency=medium

  * New upstream release reproduces case-specific data for fixed/increasing
    random number generators, and implements parameter variations when
    spread: true is specified. Man-pages are updated accordingly.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Wed, 20 Jan 2021 16:25:36 +0100

simrisc (13.03.00-1) unstable; urgency=medium

  [ Frank B. Brokken ]
  * New upstream release repairs errors inherited from the original program
    and standardizes the way Modalities are handled. The simrisc configuration
    file received several modification, which are covered by the
    simriscparams(7) man-page.

  [ tony mancill ]
  * Fix debian/watch for new GitLab listing archives format

 -- tony mancill <tmancill@debian.org>  Sat, 31 Oct 2020 08:04:52 -0700

simrisc (13.02.01-2) unstable; urgency=medium

  [ Andreas Tille ]
  * Add citation data
  * There is no autopkgtest so do not ship a README.test

  [ tony mancill ]
  * Remove citations from package Description in debian/control

 -- tony mancill <tmancill@debian.org>  Sat, 24 Oct 2020 19:58:07 -0700

simrisc (13.02.01-1) unstable; urgency=medium

  [ Frank B. Brokken ]
  * Initial release to Debian (Closes: #971823)

  [ tony mancill ]
  * Add texlive to Build-Depends
  * Update package description and doc-base
  * Update Vcs URLs for Debian Med team maintenance
  * Add patch for manpage typo

 -- tony mancill <tmancill@debian.org>  Wed, 14 Oct 2020 20:55:28 -0700
