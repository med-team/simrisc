//#define XERR
#include "group.ih"

    //  e.g.,   40 - 50:
    //          70 - * :
    // separating blanks are optional

std::istream &Group::extract(std::istream &in)
{
    char sep = 0;

    if (d_series == SINGLE)                     // with SINGLE d_begin is not
    {                                           // used. d_end is inclusive
        in >> sep;                              // maybe '*', then the max
        if (sep == '*')                         // possible value
            d_end = Globals::uint16_max;
        else
        {
            in.unget();                         // or extract the inclusive
            in >> d_end;                        // end value
        }
    }
    else
    {
        in >> d_begin >> sep >> sep;            // get '40 - 5', or '70 - *'

        if (sep == '*')
            d_end = END_AGE;                    // set end
        else
        {
            in.unget();                         // or extract the age
            in >> d_end;
        }
    }

    if (not (in >> sep) or sep != ':')      // group must end in ':'
        Err::msgTxt(Err::GROUP_NO_COLON);

    return in;                              // return
}
