//#define XERR
#include "group.ih"

bool Group::contains(double value) const
{
    return d_series == RANGE ?
                d_begin <= value and value < d_end
            :
                value <= d_end;
}
