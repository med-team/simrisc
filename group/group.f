inline Group::Group(Series series)
:
    d_series(series)
{}

inline uint16_t Group::begin() const
{
    return d_begin;
}

inline uint16_t Group::end() const
{
    return d_end;
}

template <HasGroup Type>
bool Group::nextRange(std::vector<Type> const &vect) const
{
    return  vect.empty()                    // no Type elements yet
            or
            connects(vect.back().group());
}

inline std::istream &operator>>(std::istream &in, Group &group)
{
    return group.extract(in);
}

inline bool operator!=(Group const &lhs, Group const &rhs)
{
    return not (lhs == rhs);
}
