//#define XERR
#include "group.ih"

std::ostream &operator<<(std::ostream &out, Group const &group)
{
    out << setw(3);

    if (group.d_series == RANGE)
    {
        out << group.d_begin << " - " << setw(2);

        if (
            Globals::isZero(group.d_end - END_AGE)
            or group.d_end == Globals::uint16_max
        )
            out << '*';
        else
            out << group.d_end;
    }
    else if (group.d_end == Globals::uint16_max)        // SINGLE
        out << '*';
    else
        out << group.d_end;

    return out;
}
