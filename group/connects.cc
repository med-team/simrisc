//#define XERR
#include "group.ih"

bool Group::connects(Group const &previous) const
{
    if          // subsequent SINGLE diameters must increase
    (           // subsequent RANGE d_begin must be equal to previous.d_end
        (d_series == SINGLE and d_end > previous.d_end)
        or
        (d_series == RANGE and d_begin == previous.d_end)
    )
        return true;

    Err::msg(Err::NOT_CONSECUTIVE) << d_begin << " - ..." << endl;

    return false;
}
