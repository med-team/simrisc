//#define XERR
#include "configlines.ih"

void ConfigLines::key(string const &newKey)
{
    d_tail = d_line;
    d_key = newKey;
    d_line = newKey + ' ' + d_tail;
}
