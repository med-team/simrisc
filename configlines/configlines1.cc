//#define XERR
#include "configlines.ih"

ConfigLines::ConfigLines(string const &fname)
:
    d_config(fname)
{
    d_iter = d_config.begin();
}
