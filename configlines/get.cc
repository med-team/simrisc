//#define XERR
#include "configlines.ih"

bool ConfigLines::get()
{
    if (d_redo)
    {
        d_redo = false;
        return d_iter != d_config.end();
    }

    while (d_iter != d_config.end())
    {
        string const &line = d_iter->line();

        if (line.empty() or isspace(line.front()))      // skip empty lines
        {
            ++d_iter;
            continue;
        }

        d_line = d_iter->line();
        d_lineNr = d_iter->lineNr();
        d_tail = d_iter->tail();
        d_value = d_iter->value();

        d_key = d_iter->key();
        if (d_key.empty())                              // no key -> store
            d_key = d_line;                             // the line as key

        ++d_iter;
        return true;
    }

    return false;                                       // end of file
}
