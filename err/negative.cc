//#define XERR
#include "err.ih"

// static
void Err::negative()
{
    msg(NEGATIVE) << '`' << s_lineInfo->txt << '\'' << endl;
}
