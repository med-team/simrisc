//#define XERR
#include "err.ih"

///Err Err::s_err;                         // the singleton object

LineInfo const *Err::s_lineInfo;           // last set LineInfo
bool Err::s_handle = true;                 // handle a context message

char const *Err::s_src[]
{
    "config file",
    "analysis specification",
    ""
};

char const *Err::s_plain[]              // plain msg: no line context
{
    "probabilities don't sum to 1"                  , // CUMPROB
    "Incidence carrier probabilites must sum to 1"  , // INCIDENCE_SUM_PROB
    "specification(s) missing for"                  , // MISSING_SPEC
    "multiply specified: "                          , // MULTIPLY_SPECIFIED
    "CT: unknown sensitivity for diameter "         , // CT_NO_SENS,
    "undefined specification"                       , // UNDEFINED_SPEC
    "Cumulative death proportions ("                , // CUM_DEATH
};

char const *Err::s_context[]            // line context available
{
    "parameter value too small"                 ,   // AT_LEAST
    "CT sensitivity must be 0..100 or -1"       ,   // CT_SENS
    "CT sensitivity diameters must cover 0..*"  ,   // CT_SENS_RANGE
    "Group specifications must end in :"        ,   // GROUP_NO_COLON
    "invalid Survival type"                     ,   // INVALID_TYPE
    "invalid value"                             ,   // INVALID_VALUE
    "modality repeatedly specified"             ,   // MODALITY_REPEATED
    "parameter cannot be negative"              ,   // NEGATIVE
    "age ranges not consecutive"                ,   // NOT_CONSECUTIVE
    "percentages must sum to 1"                 ,   // PROB_SUM,
    "parameter(s) must lie within 0..1"         ,   // RANGE_0_1
    "screening round ages don't increment"      ,   // ROUND_AGES_DONT_INC
    "`round: none' conflicts with "
              "`round: age' specifications"     ,   // ROUND_NONE
    "specification error"                       ,   // SPEC_ERROR
    "undefined distribution"                    ,   // UNDEFINED_DIST
    "undefined modality"                        ,   // UNDEFINED_MODALITY
    "VSD specification missing. Require "       ,   // VSD_MISSING
    "Lung cancer simulations cannot specify "       // LC_INVALID_MODALITY
                                "modality "     ,
    "Breast cancer simulations cannot specify "     // BC_CT_INVALID
                                "modality CT"   ,
    "TableParams specification error"           ,   // TABLEPARAMS
};


//     "integral number not found (parameter `"                        ,   // MISSING_NR,


//     "`tumorIncidence' probabilities must add up to 1"               ,   // INVALID_PROBSUM,


//     string{"max. number of carrier specifications ("                 +  // N_CARRIERS,
//         to_string(END_CARRIERS) + " exceeded"}.c_str()              ,

//     "`tumorGrowth:' startDiameter, Mu and Sigma not specified"      ,   // TUMORGROWTH_STARTDIAM,
//     "`tumorGrowth:' no `agegroups' defined"                         ,   // TUMORGROWTH_AGEGROUPS,
//     "at least one `tumorIncidence:' parameter line is required"     ,   // TUMORINCIDENCE_SIZE,
//     "missing `tumorSurvival' types (a, b, c and d are required)\n"  ,   // TUMORSURVIVAL_TYPES,


//     string{"max. number of carrier specifications ("                 +  // N_CARRIERS,
//         to_string(END_CARRIERS) + " exceeded"}.c_str()              ,
//     "age ranges not consecutive "                                   ,   // NOT_CONSECUTIVE,
