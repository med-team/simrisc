//#define XERR
#include "err.ih"

// static
void Err::multiplySpecified(LineInfoVect::const_iterator begin,
                            LineInfoVect::const_iterator end,
                            string const &sectionList)
{
    msg(MULTIPLY_SPECIFIED) << sectionList << ". Lines:";

    for (; begin != end; ++begin)           // list the line nrs
        emsg << ' ' << begin->lineNr << ',';

    emsg << endl;
}
