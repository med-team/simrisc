#define XERR
#include "parser.ih"

// static
VSDvect Parser::VSDparameters(VaryType varyType, StringVect const &keywords,
                              initializer_list<char> const &required)
{
    VSDvect vsdVect(required.size(), VSD{ varyType });
    auto lines = any(keywords);     // all config lines of 'keywords'

    unsigned count = 0;
    while (true)
    {
        auto const *line = lines.get();     // get the next line

        if (not line)
            break;

        VSD vsd{ varyType };

        char type;
        if (not Parser::extract(*line, type, vsd))      // get the next VSD
            continue;
                                                        // check whether the
                                                        // type (a..x) is OK
        auto idIter = find(required.begin(), required.end(), type);

        if (idIter == required.end())
        {
            Err::msg(Err::INVALID_TYPE) << '`' << type << '\'' << endl;
            continue;
        }

        ++count;                                // found a VSD specification
        vsdVect[idIter - required.begin()] = vsd;  // store the extracted VSD
    }

    if (count != required.size())
        Err::msg(Err::VSD_MISSING) << count << " specifications\n";

    return vsdVect;
}
