#define XERR
#include "parser.ih"

//static
void Parser::rmLineInfoVectors(Map &map)
{
        // element.first: the key
        // element.second: Parser *

    for (auto &element: map)                    // all elements of 'map'
    {
        if (not endPoint(element))              // there's a deeper link:
            rmLineInfoVectors(element.second->d_map);  //      continue there
        else                                    // no map is used here
            element.second->d_lines.clear();
    }
}
