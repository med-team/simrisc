#define XERR
#include "parser.ih"

    // load the configuration file 'fname'
void Parser::load(string const &fname)
{
    ConfigLines lines{ fname };

    vector<Map *> mapPtrVect{ &s_map };     // start loading at the top

    while (lines.get())                   // add the parser lines
        addConfigLine(mapPtrVect, lines, CONFIGFILE);
}
