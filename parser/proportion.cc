//#define XERR
#include "parser.ih"

bool Parser::proportion(StringVect const &base, double &dest)
{
    if (extract(one(base), dest))
    {
        if (Globals::proportion(dest))
            return true;

        Err::range();
    }

    dest = 0;
    return false;
}
