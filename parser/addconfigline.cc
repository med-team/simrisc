#define XERR
#include "parser.ih"

// static
void Parser::addConfigLine(vector<Map *> &mapPtrVect,
                           ConfigLines &configLine, ParamsSrc src)
{
                                            // accept nr-specs following
    string key = checkRedo(configLine);     // death: or riskTable:

    Map &map = *mapPtrVect.back();          // look for the key in the current
    auto iter = map.find(key);              // map section

    if (iter == map.end())                  // key was not found
    {
        locateError(mapPtrVect, configLine, src);
        return;
    }

    if (not endPoint(*iter))            // key found, but not an endpoint
    {                                   // (endpoint: next Parser is empty)
        mapPtrVect.push_back(&iter->second->d_map);  // go nesting
        return;
    }
                                        // no deeper level: handle the line

    //  --------------------------------------------------
    //                          existing
    //                  ----------------------------------
    //  new:            NONE      CONFIGFILE      ANALYSIS
    //  --------------------------------------------------
    //  CONFIGFILE      replace   add             ignore
    //
    //  ANALYSIS        replace   Exception       add
    //                            (ANALYSIS is
    //                            handled first)
    //  --------------------------------------------------

    LineInfoVect &liVect = iter->second->d_lines;

    ParamsSrc currentSrc = liVect.front().src;

                                    // replace the initialization vector
    if (currentSrc == NONE)         // by the current spec.
    {
        liVect.front() =
                LineInfo{ src, configLine.lineNr(), configLine.line(),
                               configLine.tail() };
        return;
    }
                                    // if the next line has the same ParamSrc:
    if (src == currentSrc)          // store it.
    {
        liVect.push_back      // there: store it
        (
            { src, configLine.lineNr(), configLine.line(), configLine.tail() }
        );
        return;
    }
                                    // ignore CONFIGFILE if there's
                                    // already an ANALYSIS spec.
    if (src == CONFIGFILE and liVect.front().src == ANALYSIS)
        return;

                                    // cell[1][1] in the above table
    throw Exception{} << __FILE__": internal error: ANALYSIS spec. "
                        " read after reading CONFIGFILE specs.";
}



