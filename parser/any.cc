#define XERR
#include "parser.ih"

//static
Parser::Lines Parser::any(StringVect const &keywords)
{
    Lines lines = Parser::lines(keywords);

    if (not lines)
        Err::msg(Err::MISSING_SPEC) << sectionList(keywords) << endl;

    return lines;
}
