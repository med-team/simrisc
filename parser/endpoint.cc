//#define XERR
#include "parser.ih"

// static
bool Parser::endPoint(Map::value_type const &value)
{
    return value.second->d_map.empty();   // the next Parser has an empty map
}
