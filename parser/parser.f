// static
template <Extractable ...Types>
std::istringstream Parser::extract(StringVect const &keywords, Types &...dest)
{
    return extract(one(keywords), std::forward<Types &>(dest) ...);
}

// static
template <Extractable ...Types>
std::istringstream Parser::extract(Lines &&lines, Types &...dest)
{
    if (lines)
        return extract(*lines.get(), std::forward<Types &>(dest) ...);

    std::istringstream in;
    in.setstate(std::ios::eofbit);
    return in;
}

// static
template <Extractable ...Types>
std::istringstream Parser::extract(LineInfo const &line, Types &...dest)
{
    std::istringstream in{ line.tail };

    if (not extract(in, std::forward<Types &>(dest) ...))
    {
        in.setstate(std::ios::failbit);
        Err::specification();
    }

    return in;
}

// static
template <Extractable Type>
std::istringstream Parser::extract(Lines &&lines, Type *dest, size_t size)
{
    std::istringstream in;

    if (not lines)
    {
        in.setstate(std::ios::eofbit);
        return in;
    }

    in.str(lines.get()->tail);

    for (; size--; ++dest)          // fill 'size' elements starting at dest
    {
        if (not (in >> *dest))
        {
            in.setstate(std::ios::failbit);
            Err::specification();
            break;
        }
    }

    return in;
}


// static
template <Extractable Type, Extractable ...Types>
bool Parser::extract(std::istream &in, Type &first, Types &...more)
{
    in >> first;
    return extract(in,  std::forward<Types &>(more)...);
}

inline bool Parser::extract(std::istream &in)
{
    return static_cast<bool>(in);
}


// static
template <Extractable Type>
bool Parser::one(StringVect const &base, Type &dest)
{
    return static_cast<bool>(extract(one(base), dest));
}

//template <std::floating_point ...Types>
//static bool nonNegative(StringVect const &keywords, Types &...dest)
//{
//    return extract(one(keywords)->get(), std::forward<Types &>(dest) ...);
//}

inline bool Parser::nonNegative(StringVect const &base, double &dest)
{
    return atLeast(0, base, dest);
}

template <std::integral Type>
bool Parser::nonNegative(StringVect const &base, Type &dest)
{
    double tmp;
    bool ret = nonNegative(base, tmp);
    dest = tmp;
    return ret;
}

// static
inline bool Parser::positive(StringVect const &base, double &dest)
{
    return atLeast(Globals::WEAK_TOLERANCE, base, dest);
}

inline unsigned Parser::Lines::size() const
{
    return d_size;
}

inline Parser::Lines::operator bool() const
{
    return d_size != 0;
}
