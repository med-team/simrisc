#define XERR
#include "parser.ih"

// If the previous key allows additional number-lines and a number-line is
// read next then set that line's key to the last-read key, so multiple
// lines of that key are returned.

namespace
{
    enum
    {
        INIT,
        REDO,
    };

    size_t s_state = INIT;
    string s_key;

    unordered_set<string> redo{ "death:", "riskTable:", "ageGroup:",
                                "sensitivity:", "round:", "prob:", 
                                "type:", 
                                "lung0:", "lung1:", "lung2:", "lung3:" };

    unordered_set<string> survival{ "type:", 
                                 "lung0:", "lung1:", "lung2:", "lung3:" };

    unordered_set<string> abcd{ "a",  "b",  "c",  "d", "e" };
}

//static
string Parser::checkRedo(ConfigLines &configLine)
{
    string key = configLine.key();

    if (redo.find(key) != redo.end() )      // at a key: refill with that key
    {
        s_key = key;
        s_state = REDO;
    }
    else if                         // digits always OK. '*' OK for prob:
    (                               // a..d OK voor 'survival:'
        isdigit(key.front())
        or 
        (s_key == "prob:" and key.front() == '*')
        or
        (
            survival.find(s_key) != survival.end() 
            and abcd.find(key) != abcd.end()
        )
    )
        configLine.key(key = s_key);
    else
        s_state = INIT;

    return key;
}


//    if (s_state == INIT)
//    {
//        if (redo.find(key) != redo.end() )
//        {
//            s_key = key;
//            s_state = REDO;
//        }
//    }
//    else if (                       // digits always OK. '*' OK for prob:
//                isdigit(key.front())        // a..d OK voor survival:
//                or 
//                (s_key == "prob:" and key.front() == '*')
//    )
//         configLine.key(key = s_key);
//    else
//         s_state = INIT;


