//#define XERR
#include "parser.ih"

    // for keywords A, B, C
    // where is set to: C, in section B, in section A

// static
string Parser::sectionList(StringVect const &keywords)
{
    string where = " `" + keywords.back() + '\'';

    for (auto iter = keywords.rbegin() + 1; iter != keywords.rend(); ++iter)
        where += ", in section " + *iter;

    return where;
}
