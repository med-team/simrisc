#define XERR
#include "parser.ih"

// static
LineInfoVect const &Parser::keywordsLines(StringVect const &keywords)
{
    Map const *mapPtr = &s_map;
    Map::const_iterator mapIter;

            // E.g., Scenario: iterator: returns the pointer to the Parser
            //  at Scenario's iterator: specification

    for (auto const &keyword: keywords)         // visit the range of keywords
    {
        mapIter = mapPtr->find(keyword);        // ptr to Map::value_type

        if (mapIter == mapPtr->end())       // internal error: throw exception
            throw Exception{} << "Internal error: keyword `" << keyword <<
                                 "' not configured";

        mapPtr = &mapIter->second->d_map;       // use the next keyword
    }

    // with Scenario: death: this is just one line, which may be empty,
    //  in which case it's followed by 101 cumulative death probabilities
    //  or it may contain one word, which is the name of the file containing
    //  the specifications
    return mapIter->second->d_lines;            // return the LineInfoVect
}
