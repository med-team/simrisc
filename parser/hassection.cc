#define XERR
#include "parser.ih"

// static
bool Parser::hasSection(StringVect const &keywords)
{
    return keywordsLines(keywords).front().src != NONE;
}
