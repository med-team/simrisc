#define XERR
#include "parser.ih"

// static
VSDvect Parser::VSDparameters(VaryType varyType, StringVect const &keywords,
                              size_t nRequired)
{
    VSDvect vsdVect(nRequired, VSD{ varyType });

    string values[nRequired];

                                    // one config line for 'keywords'
    if (not Parser::extract(one(keywords), values, nRequired))
        Err::msg(Err::VSD_MISSING) << nRequired << " value specifications\n";
    else
    {
        for (size_t idx = 0; idx != nRequired; ++idx)
        {
            istringstream in{ values[idx] };        // extract single values
            if (not (in >> vsdVect[idx]))           // store with SD 0 in the
            {                                       // vsdVect
                Err::msg(Err::VSD_MISSING) << nRequired <<
                                            " value specifications\n";
                break;
            }
        }
    }

    return vsdVect;
}
