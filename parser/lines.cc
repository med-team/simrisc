//#define XERR
#include "parser.ih"

// static
Parser::Lines Parser::lines(StringVect const &keywords)
{
                                                            // get the lines
                                                            // matching the
    LineInfoVect const &lines = keywordsLines(keywords);    // keywords

    return
        lines.front().src == NONE ?                         // no spec?
            Lines{ LineInfoVect{} }                         // then nothing
        :
            Lines{ lines.begin(), lines.end() };            // else return the
                                                            // specifications
}
