#define XERR
#include "parser.ih"

//extern size_t addCount;

    // load an analysis file specification
void Parser::load(std::istream &in, uint16_t startLineNr,
                  StringVect &labels,
                  OptionsVect &options)
{
    ConfigLines lines{ in, startLineNr };   // read the current stream

    setLabels(lines, labels);               // set label: specifications

    setOptions(lines, options);

    vector<Map *> mapPtrVect{ &s_map };     // start loading at the top

                                            // add the config lines
    while (lines.get())                     // to the matching Parser
        addConfigLine(mapPtrVect, lines, ANALYSIS);
}
