#define XERR
#include "parser.ih"

// static
void Parser::initLineInfoVects(Map &map)
{
        // element.first: the key
        // element.second: Parser *

        // visit all elements of 'map' (there may be none)
    for (auto &element: map)
    {
        if (not endPoint(element))              // there's a deeper link:
            initLineInfoVects(element.second->d_map);   //          continue
        else                                    // no map is used here
            element.second->d_lines.push_back(LineInfo{ NONE });
    }
}
