#define XERR
#include "parser.ih"

// OptionsVect: vector of OptionInfo elements, processed by actualize()
// OptionInfo: defined by Parser

// static
void Parser::setOptions(ConfigLines &parser, OptionsVect &optionsVect)
{
    while
    (
        parser.get()                                // got a line and it's not
        and                                         // a top-level parser
        s_map.find(parser.key()) == s_map.end()     // parameter
    )
                                                // store the option spec.
        optionsVect.push_back(                  // long option name and value
                    {
                        parser.key().back() == ':' ?    // rm a final :
                            parser.key().substr(0, parser.key().length() - 1)
                        :
                            parser.key(),
                        parser.value(),
                        parser.lineNr(),
                    }
                );

    parser.redo();             // redo the line not containing a label
}
