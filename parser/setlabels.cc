//#define XERR
#include "parser.ih"

// static
void Parser::setLabels(ConfigLines &parser, StringVect &labels)
{
    while (parser.get() and parser.key() == "label:")
        labels.push_back(parser.tail());

    parser.redo();             // redo the line not containing a label
}
