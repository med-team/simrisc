//#define XERR
#include "mri.ih"

void MRI::vInsert(ostream &out) const
{
    VSD::fmt(0, 2, 0, 3);

    out << setw(4) << ' ' << "sensitivity:  " << d_sensitivity << '\n';
    out << setw(4) << ' ' << "specificity:  " << d_specificity << '\n';
}
