//#define XERR
#include "mri.ih"

// configfile lines:
//      MRI cost:           280
//      sensitivity:            .94     0.005  Normal
//      specificity:            .95     0.005  Normal


MRI::MRI()
:
    ModBase("MRI"),
    d_sensitivity(VARY_PROB),
    d_specificity(VARY_PROB)
{
    if (not defined())
        return;

    setSpecificity();
    setSensitivity();
}
