#ifndef INCLUDED_MRI_
#define INCLUDED_MRI_

#include <iosfwd>

#include "../vsd/vsd.h"
#include "../modbase/modbase.h"

// MRI:
//     costs:          280
//          #                   value   spread  distr.
//      sensitivity:            .94     0.005  Normal
//      specificity:            .95     0.005  Normal

class MRI: public ModBase
{
    VSD d_sensitivity;
    VSD d_specificity;

    public:
        MRI();
        ~MRI() override;

    private:
        void setSensitivity();
        void setSpecificity();

        void vInsert(std::ostream &out) const override;
        double vSensitivity(size_t idx) const override;
        double vSpecificity(double age) const override;
        void vVary(std::ostream &out) override;

        void extract(char const *keyword, VSD &dest);

};

#endif
