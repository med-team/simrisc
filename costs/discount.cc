#define XERR
#include "costs.ih"

//code
double Costs::discount(double age, size_t cost) const
{
    return cost * pow(1 + d_discountProportion, d_referenceAge - age);
}
//=
