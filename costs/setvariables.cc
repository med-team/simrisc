#define XERR
#include "costs.ih"

void Costs::setVariables(bool lungCancer)
{
    if (not lungCancer)
    {
        d_base.back() = "biop:";
        Parser::nonNegative(d_base, d_usingCosts);
        d_treatment = &Costs::otherTreatment;
        return;
    }

    // below: ony used for LC simulations

            // reset d_base to read the CT: costs
    d_base.back() = "CT:";
    d_base.push_back("costs:");

    d_treatmentCosts.resize(2);
    d_treatment = &Costs::lcTreatment;
    setLungCancer();
}


//        d_using     = &Costs::lcUsing;
//        d_screening = &Costs::lcScreening;

//        d_using     = &Costs::otherUsing;
//        d_screening = &Costs::otherScreening;
