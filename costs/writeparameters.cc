//#define XERR
#include "costs.ih"

void Costs::writeParameters(ostream &out) const
{
    Globals::setWidthPrec(out, 2, 1) <<
        "Costs:\n"
        "  biopsy:          " << setw(4) << d_usingCosts << "\n"
        "  Discount:\n"
        "    reference age: " << setw(4) << d_referenceAge << '\n';

    Globals::setWidthPrec(out, 4, 3) <<
        "    proportion:    " << setw(5) << d_discountProportion << "\n"
        "  diameters:\n";

    Globals::setWidthPrec(out, 2, 0);

    for (size_t idx = 0; idx != d_treatmntPair.size(); ++idx)
        out <<
        "    diameter >= " <<
                setw(6) << d_treatmntPair[idx].first << " mm: " <<
                setw(4) << d_treatmntPair[idx].second << '\n';

    out.put('\n');
}
