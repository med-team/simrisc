#define XERR
#include "costs.ih"

void Costs::setLungCancer()
{
    size_t unused;            // = modbase->cost(), extracted by ModBase

    if (not Parser::extract(Parser::one(d_base), unused,
                            d_usingCosts,
                            d_treatmentCosts[0], d_treatmentCosts[1]))
        Err::atLeast(0);
}
