inline double Costs::usingAt(double age) const
{
    return discount(age, d_usingCosts);
}

inline double Costs::screening(double age, size_t cost) const
{
    return discount(age, cost);
}

inline double Costs::treatment(double age, double diameter,
                                            bool metastasis) const
{
        // calls lcTreatment with lung cancer, otherwise: otherTreatment
    return (this->*d_treatment)(age, diameter, metastasis);
}

//    return (this->*d_using)(age, d_usingCosts);
//    return (this->*d_screening)(age, cost);
