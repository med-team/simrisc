#define XERR
#include "costs.ih"

Costs::Costs()
:
    d_base{ ""}
{
    bool lungCancer = not Globals::isBreast();

    setDiameters();
    setDiscount(lungCancer);

    setVariables(lungCancer);           // d_base, d_using, d_screening,
                                        // d_treatment
}
