#define XERR
#include "densities.ih"

//  #                bi-rad:  a      b      c      d
//  ageGroup:    0  - 40:    .05    .30    .48    .17

void Densities::add(bool *checkRange, LineInfo const &line)
{
    ProbGroup density{ RANGE };

    if (not Parser::extract(line, density))         // density spec. error
    {
        *checkRange = false;
        return;
    }

//xerr("ProbGroup: " << density);

    Err::Context context;

    if (not percentages(density.prob()))
        context = Err::RANGE_0_1;

    else if (not density.sumOne())
        context = Err::PROB_SUM;

    else                                            // so far all's OK
    {
        if (*checkRange)
        {
            if (density.group().nextRange(d_densities))
                d_densities.push_back(density);
            else
                *checkRange = false;
        }
        return;
    }

    Err::msgTxt(context);
}
