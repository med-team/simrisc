#ifndef INCLUDED_DENSITIES_
#define INCLUDED_DENSITIES_

//  example config spec.:

// BreastDensities:
//
//     #                bi-rad:  a      b      c      d
//     ageGroup:    0  - 40    .05    .30    .48    .17
//     ageGroup:    40 - 50    .06    .34    .47    .13
//     ageGroup:    50 - 60    .08    .50    .37    .05
//     ageGroup:    60 - 70    .15    .53    .29    .03
//     ageGroup:    70 - *     .18    .54    .26    .02

#include <iosfwd>

#include "../probgroup/probgroup.h"
#include "../typedefs/typedefs.h"

class Densities
{
    StringVect  d_base;
    ProbGroup::Vector d_densities;  // contains the ageGroup data

    public:
        Densities();

        ProbGroup::Vector const *biRad() const;


        void writeParameters(std::ostream &out) const;

    private:
        void add(bool *checkRange, LineInfo const &ageGroup);

        bool nextRange(ProbGroup const &next) const;

        static bool percentages(std::vector<double> const &vect);
};

inline ProbGroup::Vector const *Densities::biRad() const
{
    return &d_densities;
}

                                                // birad idx for age, given
                                                // its probability
//        uint16_t biradIdx(double age, double prob) const;

                                        // randomly determined bi-rad indices
                                        // for screening round ages
//        Uint16Vect biRadIndices(DoubleVect const &ages) const;

#endif
