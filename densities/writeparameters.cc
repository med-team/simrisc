//#define XERR
#include "densities.ih"

void Densities::writeParameters(std::ostream &out) const
{
    out <<
        "BreastDensities:\n";

    Globals::setWidthPrec(out, 4, 3);

    for (ProbGroup const &density: d_densities)
        out << setw(2) << ' ' << density << '\n';

    out.put('\n');
}
