//#define XERR
#include "densities.ih"

Densities::Densities()
:
    d_base{ "BreastDensities:", "ageGroup:" }
{
    if (Globals::simulationType() != BREAST)
        return;

                                // retrieve the Densities: ageGroup: lines
    auto lines = Parser::any(d_base);

    bool checkRange = true;     // check whether age ranges connect.

    while (true)
    {
        LineInfo const *line = lines.get();
        if (line == 0)
            break;

        add(&checkRange, *line);    // some failure: don't check ranges
    }
}
