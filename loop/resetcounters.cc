//#define XERR
#include "loop.ih"

void Loop::resetCounters()
{
    d_totalCost = 0;                        // org: womenCosts
    d_sumDeathAge = 0;                      // org: womenYears

    d_nIntervals = SizeVect(d_nRounds);
    d_nTrueIntervals = SizeVect(d_nRounds);
    d_nRoundFP = SizeVect(d_nRounds);    // # of false-positives per round
    d_nRoundFN = SizeVect(d_nRounds);    // # of false-positives per round
    d_nDetections = SizeVect(d_nRounds);
    d_roundCost = DoubleVect(d_nRounds);

    d_modalities.resetCounters(d_nRounds);
}
