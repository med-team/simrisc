#define XERR
#include "loop.ih"

extern size_t g_caseIdx;

void Loop::screening()
{
    if (d_status == PRESENT and d_nRounds > 0)
        g_log << "\nLoop::screening starts\n";

        // d_round is a data member: available in all of Loop's members
    for (d_round = 0; d_status == PRESENT and d_round != d_nRounds; ++d_round)
    {
        double screeningAge = d_screening.roundAge(d_round);

        g_log << __FILE__ << " screening round " << d_round << " at age " <<
                screeningAge << '\n';
        
        if (leaving(screeningAge))      // the case died or detected a tumor
            return;

        screen(screeningAge);           // perform the screening
    }
}


//if (g_caseIdx == 102)
//xerr("screening " << g_caseIdx << " at age " << screeningAge);


//if (g_caseIdx == 102)
//xerr("screening " << g_caseIdx << " LEAVES at age " << screeningAge);

