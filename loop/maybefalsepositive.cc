#define XERR
#include "loop.ih"

    // no tumor is present

void Loop::maybeFalsePositive(ModBase *modBase, double screeningAge)
{
    double rnd = Random::instance().uniform();
    double spec = modBase->specificity(screeningAge);

    if (rnd <= spec)
        g_log << "      " << rnd << " <= specificity(" <<
                screeningAge << ") = " << spec << ": no false positive\n";
    else
    {
        g_log << "      " << rnd << " > specificity(" <<
                screeningAge << ") = " << spec << ": false positive\n";

        modBase->falsePositive();
        ++d_nRoundFP[d_round];
        d_roundInfo[d_round] = FALSE_POSITIVE;

            // add the using costs to the case and biop costs
        d_caseCost += addBiopCosts(d_costs.usingAt(screeningAge));
    }
}
