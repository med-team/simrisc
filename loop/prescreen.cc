//#define XERR
//#include "loop.ih"
//
//void Loop::preScreen()
//{
//    // see prescreen.txt
//
//    if (d_nRounds == 0)         // no scenario rounds, no pre-screening
//        return;
//
//    g_log << "\nLoop::preScreen\n";
//
//    // age 0: the 1st screening age, e.g. 50
//    double age0 = d_screening.roundAge(0);  // using the complement of the
//    if (                                    // prescreen in the original code
//        d_naturalDeathAge >= age0
//        and
//        (
//            not d_tumor.selfDetectable()    // these are accessor calls
//            or
//            d_tumor.selfDetectAge() >= age0
//        )
//    )
//    {
//        g_log << "   case alive and no (detectable) tumor before age " <<
//                 age0 << ": no preScreen\n";
//        return;
//    }
//
////  ---------------------------------------------------------------------
////                                        action:
////  self-       nat.death <         -----------------------
////  detectable  tumor:selfDetect    nat.death   tumordetect   alternative
////  ---------------------------------------------------------------------
////   0             0                    1            0           if
////   0             1                    0            1           if
////   1             0                    1            0           else
////   1             1                    1            0           if
////  ---------------------------------------------------------------------
//
//    // main block resulting in the case leaving when prescreening
//    // ==========================================================
//
//    if (                        // pre-screening is performed:
//        not d_tumor.selfDetectable() or
//        (
//            d_tumor.selfDetectable()
//            and
//            d_naturalDeathAge < d_tumor.selfDetectAge()
//        )
//    )
//    {
//        g_log << "  *** preTumorDeath: case died naturally\n";
//        preTumorDeath();    // natural death: no tumor or not self-detected
//    }
//    else
//    {
//        g_log <<
//            "  *** self detected: case self-detected a tumor and leaves\n";
//        preSelfDetected();  // self detected the tumor before the screening
//    }
//}
//
