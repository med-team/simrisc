//#define XERR
#include "loop.ih"

// with BreastDensities: compute the bi-rad category to use (a: 0 .. d: 3)
// with LC: a 0-initialized vector of d_nRounds elements

Uint16Vect Loop::cptIndices() const
{
    Uint16Vect ret;

    if (not Globals::isBreast())    // LC: a 0-initialized vector is used
        ret.resize(d_nRounds);

    else                        // for BC: compute the indices used to obtain
    {                           //         dose values
        double prob = Random::instance().uniform();

        for (double age: d_screening.ages())
        {
                    // d_groupVector initialized at the constructor to
                    // the LC s3 table or the Densities biRad vector.
            RowCol rowCol = ProbGroup::probIndexOf(*d_groupVector, age, prob);

                                                        // invalid age
            if (rowCol.first == static_cast<uint16_t>(string::npos))
                    throw Exception{} <<
                                "runtime-error: no age category for age " <<
                                age;

                    // store the birad/s3 column index to use for prob/age:
            ret.push_back(rowCol.second);
        }
    }

    return ret;
}
