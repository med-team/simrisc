#define XERR
#include "loop.ih"

bool Loop::falseNegative(ModBase *modBase)
{
    // modified using Jing Wang's email (Tue, 20 Oct 2020 16:04:42 +0000)
    if (
        double uval = Random::instance().uniform(),
               sval = sensitivity(modBase);
        //uval > sval
        sval < uval
    )
    {
        g_log << "    detected FALSE_NEGATIVE (" << uval <<
                    " > sensitivity (" << sval << ")\n";
        ++d_nRoundFN[d_round];
        d_roundInfo[d_round] = FALSE_NEGATIVE;
        return true;
    }

    return false;           // not a false negative conclusion
}
