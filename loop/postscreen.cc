#define XERR
#include "loop.ih"

void Loop::postScreen()
{
    if (d_status != PRESENT)            // the woman has died: no further
        return;                         // actions

    g_log << "\nLoop::postScreen\n";

    // C1
    if (not d_tumor.at(END_AGE))        // no tumor beyond the last screening 
    {                                   // round
        left(NATURAL_POST, d_naturalDeathAge);
        return;
    }
            // tumor beyond the last screening age: determine its
            // characteristics 
    // C2
    d_tumor.characteristicsAt(d_tumor.selfDetectAge());
    d_tumor.setDeathAge();

    if (d_tumor.selfDetectBefore(d_naturalDeathAge))
        d_caseCost += treatmentCosts(d_tumor.selfDetectAge());

    d_deathAge = min(d_naturalDeathAge, d_tumor.deathAge());

    left(
        d_tumor.selfDetectBefore(d_naturalDeathAge) ? 
            SELF_POST  : 
            d_naturalDeathAge < d_tumor.onset() ? 
                NATURAL_POST :
                UNDETECTED_POST,
        d_deathAge);
         // d_tumor.selfDetectAge());
}


////////////////////////////////////////////////////////////////////
    
//
//
//
//    // main consequence block when self-detecting a tumor after the screenings
//    // vvvvvvvvvvvvvvvvv            // there may be a tumor, detected before
//    if (                            // the woman's natural death
//        d_tumor.selfDetectable()
//        and d_tumor.selfDetectAge() < d_naturalDeathAge
//    )
//    {
//                                // this results in a tumor caused death.
//                                // (the condition check in the original
//                                //  sources is superfluous)
//        g_log << "  *** case died because of a tumor detected at age " <<
//                 d_tumor.selfDetectAge() << '\n';
//
//        d_roundDetected = d_screening.nRounds();
//        characteristics(NATURAL_POST, SELF_POST);
//    }
//    else                        // or a naturally caused death
//    {
//        //if (g_caseIdx >= g_err)xerr(g_caseIdx << " setStatus");
//        setStatus(NATURAL_POST, d_naturalDeathAge);
//        g_log << "  *** case died at natural death age " <<
//                                                d_naturalDeathAge << '\n';
//
//                                    // although the woman has died, still
//                                    // determine the tumor's characteristics
//        if (d_tumor.selfDetectable())
//            d_tumor.characteristicsAt(d_deathAge);
//    }
