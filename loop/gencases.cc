#define XERR
#include "loop.ih"

size_t g_caseIdx;           // -> loop.ih       if (caseIdx > 24842)
size_t g_err = 14;

void Loop::genCases(size_t iter, size_t nCases)
{
    CSVTable dataTable = headerData(iter);

    bool showAll = not d_options.specificAges() and d_options.lastCase() == 0;

                                        // perform 'nCases' simulations
    for (size_t caseIdx = 0; caseIdx != nCases; ++caseIdx)
    {
        g_caseIdx = caseIdx;

        g_log.caseIdx(caseIdx);
        caseInit();             // initialize the data for this case

        screening();        // no action unless screening rounds are specified

        postScreen();

        d_sumDeathAge += d_deathAge;
        d_totalCost += d_caseCost;

        if (dataTable.stream() and (showAll or caseIdx + 1 == nCases))
        {
            d_tumor.checkDeath(d_naturalDeathAge);
            writeData(dataTable, caseIdx);
        }
    }

    dataTable << hline();
}

// In the original womanloop function a test is performed whether the
// woman has died or not. This test is superfluous because if the woman enters
// postScreen with status PRESENT then in postScreen the status either changes
// to NATURAL_POST or SELF_POST.
