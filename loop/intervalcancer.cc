#define XERR
#include "loop.ih"

    // a self-detected tumor before screeningAge or natural death: 
    // an interval cancer.
    // the tumor characteristics (at self-detection age) have already been
    // determined in loop/leaving.cc.

bool Loop::intervalCancer() // (double screeningAge)
{
    d_tumor.intervalCancer();               // sets d_interval true
    ++d_nIntervals[d_round];

    if (d_screening.attend(d_round))
        ++d_nTrueIntervals[d_round];

        // treat the tumor and determine the death-age of the women
        // since the tumor is present, no need to call diameterCheck()
//    addCost(treatmentCosts(d_tumor.selfDetectAge()));

//    d_tumor.setDeathAge();

    return left(SELF_DURING, 
                min(d_naturalDeathAge, d_tumor.deathAge()));
}
