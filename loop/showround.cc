#define XERR
#include "loop.ih"

string Loop::showRound() const
{
    switch (d_roundDetected)
    {
        case -1:                            // no tumor detected
        return "0"s;

        case 0:                             // tumor detected before the
        return "-1"s;                       // 1st screening round

        default:
        return (not d_screening.attend(d_roundDetected - 1) ? '*' : ' ')  +
                to_string(d_roundDetected);
    }
}
