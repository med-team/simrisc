#define XERR
#include "loop.ih"

bool Loop::left(Status status, double deathAge)
{
    d_status = status;
    d_deathAge = deathAge;
    
    g_log << s_status[status] << '\n';

    return true;
}
