#define XERR
#include "loop.ih"

Loop::Loop(StringVect const &labels)
:
    d_options(Options::instance()),
    d_labels(labels),
    d_tumor(d_tumorInfo),
    d_modalities(d_tumor),
    d_screening(d_modalities),
    d_nRounds(d_screening.nRounds()),
    d_firstRoundAge(d_nRounds == 0 ? 0 : d_screening.roundAge(0)),
    d_timestamp(DateTime{ DateTime::LOCALTIME }.rfc2822()),
    d_tnm(d_options.tnm() ? &fillTNM : &noTNM)
{
    if (Globals::isBreast())
        d_groupVector = d_densities.biRad();
}
