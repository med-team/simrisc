#define XERR
#include "loop.ih"

CSVTable Loop::headerRounds(size_t iteration) const
{
   CSVTable tab{ outStream(
        d_nRounds == 0 ? "" : d_options.roundsFile(), iteration),
                 "  " };

//          false             number of                      number of
//         ---------  ------------------------  screening  --------------
//  round  pos. neg.  tumors  interval trueInt   costs     Mammo Tomo MRI
//  ---------------------------------------------------------------------

    if (not tab.stream() or d_nRounds == 0)
        return tab;

    labels(tab.stream());

    tab.fmt() << "round" <<                             // 1: round
                "pos." << "neg." <<                     // 2-3
                "tumors" << "interval" <<               // 4-5
                "trueInt" << "screening" << "biop567yyy";  // 6-8

        //  E.g., "Mammo" << "Tomo" << "MRI"; // 9-11 (generally: 9..)

    size_t nFields = d_modalities.roundFmt(tab); 

    tab << hline();
    tab.row(1) << join(2, FMT::CENTER) << "false"       <<  //  2-3
                  join(3, FMT::CENTER) << "number of"   <<  //  4-6 
                  join(2, FMT::CENTER) << "costs"       <<  //  8
                  join(nFields, FMT::CENTER) << "number of";

    tab.row(1) << hline(2) << hline(3) << hline(2) << hline(nFields);  

    tab.more() << "round" << "pos." << "neg." <<        // 1-3
                "tumors" << "interval" << "trueInt" <<  // 4-6
                "screening" << "biop";                  // 7-8

    d_modalities.roundHeaders(tab);

    tab.row();

    tab << hline();

    tab.sep(", ");

    return tab;
}
