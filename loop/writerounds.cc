#define XERR
#include "loop.ih"

void Loop::writeRounds(CSVTable &tab) const
{
    if (not tab.stream() or d_nRounds == 0)
        return;

    for (size_t rnd = 0; rnd != d_nRounds; ++rnd)
    {
        tab.more() << (rnd + 1) <<          // rounds.txt: natural round nrs
                      d_nRoundFP[rnd] << d_nRoundFN[rnd] <<
                      d_nDetections[rnd] << 
                      d_nIntervals[rnd] << d_nTrueIntervals[rnd] << 
                      static_cast<size_t>(round(d_roundCost[rnd])) <<
                      static_cast<size_t>(round(d_roundBiopCosts[rnd]));

        d_modalities.writeRounds(tab, rnd);
        tab.row();
    }

    tab << hline();
}
