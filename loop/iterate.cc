#define XERR
#include "loop.ih"

void Loop::iterate()
{
    CSVTable sensTable = headerSensitivity();   // table to collect
                                                // sensitivity values

    Random::instance().setSeed(d_scenario.seed());

    d_modalities.active();                      // prepare the active Mods.

    for (size_t idx = 0, end = d_scenario.nIterations(); idx != end; ++idx)
    {
        ofstream spreadStream;

        size_t nCases = cases();

        d_roundBiopCosts.assign(d_nRounds, 0);  // reset the rounds biop costs

        Random::instance().reinit(nCases, d_scenario.generatorType());

        if (Globals::vary())                    // maybe vary the parameters
        {
            spreadStream = outStream(d_options.spreadFile(), idx);
            labels(spreadStream);

            d_modalities.vary(spreadStream);    // vary the used modalities
            d_tumorInfo.vary(spreadStream);     // vary Beir7, Growth,
                                                // Incidence, and Survival
        }

        writeParameters(idx);       // write the actual parameter values,

        CSVTable rounds = headerRounds(idx);

        resetCounters();

        genCases(idx, nCases);

        writeRounds(rounds);        // -> ORG/loopendout.cpp

        writeSensitivity(sensTable, idx);
    }

    if (sensTable.stream())
        sensTable << hline();
}

