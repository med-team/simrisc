#define XERR
#include "loop.ih"

void Loop::writeData(CSVTable &tab, size_t idx) const
{
    static Status detection[] = { SELF_PRE, SELF_DURING, SELF_POST };

                            // case numbers in data*.txt are now natural nrs.
    tab.more() << (idx + 1) <<                                  // 1
           (d_deathAge == d_naturalDeathAge ? "Natural" 
                                            : "Tumor") <<       // 2
           d_deathAge << d_naturalDeathAge << d_status;         // 3 - 5

    d_tumor.writeData(tab);                                     // 6 - 14

    ostringstream out;
    if (Globals::isZero(d_tumor.diameter()))
        (*d_tnm)(out);
    else
    {                                                   // set the tumor S3
        RowCol rowCol = d_tumor.rowCol();               // row/col. For BC
        out << rowCol.first << ',' << rowCol.second;    // row is bc: TNM, col
    }                                                   // = 0 (via Survival::
                                                        // setVSD
        // self  round
        // 16    17
        //       **
    tab << 
            static_cast<size_t>(round(d_caseCost)) <<                // 15
            static_cast<size_t>(round(d_biopCosts)) <<            // 15-pre
           (find(detection, detection + size(detection), d_status) != 
                          detection + size(detection)) <<           // 16
           showRound() << d_roundInfo <<                            // 17 - 18
           out.str();                                               // 19
}



