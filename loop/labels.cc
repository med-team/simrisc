//#define XERR
#include "loop.ih"

void Loop::labels(ostream &out) const
{
    out << "Analysis " << d_timestamp <<
                        " (SimRisc V. " << Icmake::version << ")\n";

    for (auto const &line: d_labels)
        out << line << '\n';

    out.put('\n');
}
