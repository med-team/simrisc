//#define XERR
#include "loop.ih"

double Loop::addBiopCosts(double costs)
{
    d_biopCosts += costs;
    d_roundBiopCosts[d_round] += costs;
    return costs;
}
