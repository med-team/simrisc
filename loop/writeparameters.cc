#define XERR
#include "loop.ih"

void Loop::writeParameters(size_t iteration) const
{
    ofstream out{ outStream(d_options.parametersFile(), iteration) };

    if (not out)
        return;

    labels(out);            // timestamp and scenatio explanatory labels

    setPrecision(out, 3);

    d_scenario.writeParameters(out, iteration + 1);
    d_costs.writeParameters(out);
    if (Globals::isBreast())
        d_densities.writeParameters(out);
    d_modalities.writeParameters(out);
    d_screening.writeParameters(out);
    d_tumorInfo.writeParameters(out);
}
