#define XERR
#include "loop.ih"

extern size_t g_caseIdx;

void Loop::caseInit()
{
    g_log << "\nLoop::caseInit\n";

    d_caseCost = 0;                         // org: womenCosts
    d_roundDetected = -1;                   // org: roundDetected
    d_roundInfo.assign(d_nRounds, '0');     // attendance info per round per
                                            // case at [idx] the info of round
                                            // idx is stored

    d_biopCosts = 0;

    //xerr("cptIndices");
                                // BC simulation: bi-rad column indices or
    d_indices = cptIndices();   // LC simulation: 0-vector

    //xerr("cumTotalRisk");
    d_tumorInfo.cumTotalRisk(                   // 24/1 also sets the attended 
                    d_screening.radiationRisk(  // 24/1 flag
                            d_modalities,
                            d_indices,
                            d_tumorInfo.beir7().beta(),
                            d_tumorInfo.beir7().eta()
                    )
                );

        // naturalDeathAged is called to standardize the Random::uniformCase
        // calls
    d_options.fixedNaturalDeathAge(d_naturalDeathAge = naturalDeathAge());

    d_deathAge = d_naturalDeathAge;     // iniial deathAge estimate.

    d_status = PRESENT;

    d_tumor.reset();            // determine this case's tumor parameters

    g_log << "\n"
        "case init:\n"
        "   Natural death age: " << d_naturalDeathAge << '\n' <<
        d_tumor << '\n';
}
