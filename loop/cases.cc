#define XERR
#include "loop.ih"

        // if last case (--nCases) was specified, use that number of cases
        // if death-age (--death-age) was specified simulate one case having
        // the specified death age. Otherwise use nWomen cases

size_t Loop::cases() const
{
    return
        d_options.lastCase() != 0 ? d_options.lastCase()    :
        d_options.specificAges()  ? 1                     :
                                    d_scenario.nCases();
}
