//#define XERR
#include "loop.ih"

    // true: this modality is used

    // d_round contains the screening round index


bool Loop::use(ModBase *modBase)
{
    bool attends = d_screening.attend(d_round);                 // 24/1

//    if (d_screening.rate() < Random::instance().uniform())    // 24/1
    if (not attends)                                            // 24/1
        return false;

// xerr("round " << d_round << ", count " << d_round);

    d_roundInfo[d_round] = ATTENDED;

    modBase->count(d_round);               // screen around line 22 ->
    return true;
}


// ----------------------------------------------------------------------
//
//// screen: line 26: densities not used anymore
//
//    // uint16_t density = d_indices[d_round];
//
//    // NOTE: in the orig. source density NRs are used: 1..4
//    //       here density INDICES are usd: 0..3
//
//    if (                use Modalities member:
//        modality == MAMMOGRAPHY and d_screening.round(d_round).mammo()
////        and (density == 0 or density == 1)    //
//    )
//    {
//        ++d_nMam[d_round];
//        return true;
//    }
//
//    if (
//        modality == TOMOSYNTHESIS and d_screening.round(d_round).tomo()
////        and (density == 2 or density == 3)
//    )
//    {
//        ++d_nTomo[d_round];
//        return true;
//    }
//
//    return false;


//     return RandomPool::instance().attendance(d_round * (modality + 1)
//                <= d_screening.rate().value()
//             and
//             ...

//    (
//        rndPoolAttendance[ screeningRound * (modality + 1) ]
//        <=
//        attendanceRate
//    )
//    &&
//    (
//        (
//            ( modality == 0) &&
//            (scr[screeningRound].T0) &&
//            (
//                (density[screeningRound] == 1)
//                ||
//                (density[screeningRound] == 2)
//            )
//        )
//        ||
//        (
//            ( modality == 1) &&
//            (scr[screeningRound].T1) &&
//            (
//                (density[screeningRound] == 3) ||
//                (density[screeningRound] == 4)
//            )
//        )
//    )
