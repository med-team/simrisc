#define XERR
#include "incidence.ih"

void Incidence::setIncidence(StringVect &base, size_t idx, bool setProb)
{
    base.back() = "riskTable:";

    ParamsPtr ptr{
            Parser::hasSection(base) ?
                static_cast<Params *>(new TableParams{ base, idx, setProb })
            :
                static_cast<Params *>(new VSDParams{ base, idx, setProb })
          };

    if (ptr->prob() >= 0)
        d_params.push_back(move(ptr));
}

//===========================================================
//    ParamsPtr ptr;
//
//    if (not Parser::hasSection(base))
//        ptr.reset(new VSDParams{ base, idx, setProb });
//    else if (onlyRiskTable(base))
//        ptr.reset(new TableParams{ base, idx, setProb });
//    else
//        return;
//
//    }

