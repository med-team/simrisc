#define XERR
#include "incidence.ih"

void Incidence::writeParameters(ostream &out) const
{
    out << "    Incidence:\n";

    auto simulationType = Globals::simulationType();

    for (
        size_t idx = 0, end = simulationType == BREAST ? 3 : 1;
            idx != end;
                ++idx
    )
    {
        string label;
        switch (simulationType)
        {
            case BREAST:
                label = Params::label(idx);
            break;

            case FEMALE:
            case MALE:
                label = Globals::label(simulationType);
            break;
        }

        d_params[idx]->writeParameters(out);
    }
}




