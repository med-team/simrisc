#ifndef INCLUDED_INCIDENCE_
#define INCLUDED_INCIDENCE_

//  Either Male, Female, or Breast plus the BRCA specs.
//
// Tumor:
//     Incidence:
//         Male:
//              specifying 'riskTable' takes precedence: must be pairs of
//              age proportion values. The ages must be increasing and must
//              be >= 0 and <= MAX_AGE (-> enums)
//
//              riskTable: 25 0.001 
//                          30 0.003 35 0.005 
//                          40 0.009 45 0.014

//             #                   value   spread  distr.
//             lifetimeRisk:         .22   .005    Normal
//             meanAge:            72.48  1.08     Normal
//             stdDev:              9.28  1.62     Normal
//
//         Female:
//             #                   value   spread  distr.
//             lifetimeRisk:         .20   .004    Normal
//             meanAge:            69.62  1.49     Normal
//             stdDev:              9.73  1.83     Normal
//
//         Breast:
//             probability:    1
//             #                   value   spread  distr.
//             lifetimeRisk:         .226  .0053   Normal
//             meanAge:            72.9    .552    Normal
//             stdDev:             21.1
//
//         BRCA1:
//             probability:    0
//             #                   value   spread  distr.
//             lifetimeRisk:         .96
//             meanAge:            53.9
//             stdDev:             16.51
//
//          BRCA2:
//             probability:    0
//             #                   value   spread  distr.
//             lifetimeRisk:         .96
//             meanAge:            53.9
//             stdDev:             16.51

//  as in the original sources: with 0 probabilities the specified values
//  are set to 0


#include <vector>
#include <string>
#include <memory>

#include "../typedefs/typedefs.h"

#include "../params/params.h"

class Incidence
{
    using ParamsPtr = std::unique_ptr<Params>;

    std::vector<ParamsPtr> d_params;
    DoubleVect d_cumProb;
    DoubleVect2 d_tumorRisk;        // tumor risks per incidence category

    public:
        Incidence();

        void vary(std::ostream &out);            // vary (d_vsd's) values

                                        // was: carrier() MODIFY for LC
        uint16_t index() const;         // randomly selected carrier index

        DoubleVect const &tumorRisk(size_t idx) const;                  // .f

        void writeParameters(std::ostream &out) const;

    private:
        void cptTumorRisk();            // assign values to d_tumorRisk

                                   // 'incidence' originally named 'carrier'
        void setIncidence();        // sets d_params                    // 1

        void setIncidence(StringVect &base, size_t idx, bool setProb);  // 2

        static bool valid(double sumProb);                              // .ih
        double sumProbs();

// not used:        static bool onlyRiskTable(StringVect &base);

        static void writeCarrier(std::ostream &out, size_t idx,
                                 char type, double value);
};

#include "incidence.f"

#endif
