//#define XERR
#include "incidence.ih"

double Incidence::sumProbs()
{
    double sumProb = 0;

    for (ParamsPtr const &paramsPtr: d_params)
    {
        sumProb += paramsPtr->prob();
        d_cumProb.push_back(sumProb);
    }

    return sumProb;
}
