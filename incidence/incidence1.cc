#define XERR
#include "incidence.ih"

Incidence::Incidence()
{
    setIncidence();

        // compute the tumor risks per category for END_AGE elements

                                            // room for the age-risks per
    d_tumorRisk.resize(d_params.size());    // carrier category

    for (auto &vect: d_tumorRisk)           // resize for all ages
        vect.resize(END_AGE);

    cptTumorRisk();                         // then set the tumor risks

}
