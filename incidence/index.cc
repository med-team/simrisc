//#define XERR
#include "incidence.ih"

uint16_t Incidence::index() const
{
    if (d_cumProb.size() == 1)
        return 0;

    double value = Random::instance().uniform();

    return find_if(d_cumProb.begin(), d_cumProb.end(),
                [&](double cumProb)
                {
                    return value <= cumProb;
                }
            ) - d_cumProb.begin();
}
