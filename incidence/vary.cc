//#define XERR
#include "incidence.ih"

void Incidence::vary(ostream &out)
{
    out << "  Incidence:\n";

    for (ParamsPtr &paramsPtr: d_params)
    {
                                                    // if this incidence spec.
        if (paramsPtr->prob() >= Globals::TOLERANCE)    
        {                                           // is used, then vary the
            paramsPtr->vary(out);                   // distribution's value
            out.put('\n');
        }
    }

    cptTumorRisk();
}
