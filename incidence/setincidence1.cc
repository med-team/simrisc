#define XERR
#include "incidence.ih"

    // Incidence parameters: Male, Female, Breast.
    // BRCA1 and BRCA2 are configured, but not used because their
    // probabilities are 0.

void Incidence::setIncidence()
{
    SimulationType simulationType = Globals::simulationType();

    StringVect base;

    switch (simulationType)
    {
        case BREAST:
            base.resize(2);                         // set [0] and [1] in
        break;                                      // setincidence2.cc

        case MALE:
            base.push_back(Globals::label(MALE));
        break;

        case FEMALE:
            base.push_back(Globals::label(FEMALE));
        break;
    }

    if (simulationType != BREAST)                   // LC simulation
    {
        base.push_back("");
        setIncidence(base, 0, false);               // no probability
        d_cumProb.push_back(1);
    }
    else
    {
                // Breast:, currently: BRCA1:, BRCA2:
        for (size_t idx = 0, end = Params::nLabels(); idx != end; ++idx)
        {
            base[0] = Params::label(idx);
            setIncidence(base, idx, true);          // set probability
        }

        if (not valid(sumProbs()))                  // probs must sum to 1
        {
            Err::msg(Err::INCIDENCE_SUM_PROB) << endl;
            return;
        }
    }
}
