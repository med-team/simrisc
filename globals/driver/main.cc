#include <iostream>
#include <algorithm>

#include "../globals.h"

using namespace std;

int main()
{
    DoubleVect dv{.1, .1, .5, .5, .5, .7, .8, .9, 1};

    while (true)
    {
        cout << "prob? ";
        double prob;
        cin >> prob;
        cin.ignore(100, '\n');


        auto begin = dv.begin();
        auto last = upper_bound(begin, dv.end(), prob);
        auto first = lower_bound(begin, last, *(last - 1));

        cout << (first - begin) << ", " << (last - begin) << '\n' <<
                Globals::findAge(dv, prob) << '\n';
    }
}
