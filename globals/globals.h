#ifndef INCLUDED_GLOBALS_
#define INCLUDED_GLOBALS_

#include <ostream>
#include <iomanip>
#include <vector>

#include "../typedefs/typedefs.h"

class Globals
{
    static bool s_vary;
    static SimulationType s_simType;
    static unsigned s_width;
    static char const *s_label[][3];

    public:
        static uint16_t const uint16_max;

        static constexpr double TOLERANCE = 1e-8;
        static constexpr double WEAK_TOLERANCE = 1e-3;
        static constexpr double NO_TUMOR = -1;          // tumor/

        static double const s_sqrt2PI;

        static char const *defaultSimType();                            //  .f

        static double findAge(DoubleVect const &cumProb, double prob);
        static bool findSimType(std::string const &value);

        static bool isBreast();                     // simType == BREAST    .f
        static bool isMale();                       // simType == MALE      .f

        static bool isPositiveZero(double value);                       //  .f
        static bool isZero(double value);                               //  .f

        static char const *label(SimulationType type);                  //  .f

        static bool proportion(double const &value);                    //  .f

        static void setSimType(std::string const &analysis);            //  .f
        static void setVary(bool vary);                                 //  .f

        static std::ostream &setWidthPrec(std::ostream &out,
                                          unsigned width, unsigned precision);

        static char const *simTypeLabel(FirstChar fc);                  // .f

        static SimulationType simulationType();                         //  .f

                                                    // -1: lhs < rhs
                                                    //  0: lhs == rhs
                                                    //  1: lhs > rhs
        static int weakCompare(double lhs, double rhs); // uses WEAK_TOLERANCE

        static std::ostream &setPrecision(std::ostream &out,
                                          uint16_t nDigits);

        static bool vary();                                             //  .f
};

#include "globals.f"
#endif
