//#define XERR
#include "globals.ih"

// static
ostream &Globals::setWidthPrec(ostream &out, unsigned width,
                                             unsigned precision)
{
    if (width != 0)
    {
        out.width(width);
        setPrecision(out, precision);
    }

    return out;
}
