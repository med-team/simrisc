//#define XERR
#include "globals.ih"

// static
int Globals::weakCompare(double lhs, double rhs)
{
    lhs -= rhs;
    return lhs < -WEAK_TOLERANCE ? -1 :
           lhs > WEAK_TOLERANCE  ?  1 :
                                    0;
}
