//#define XERR
#include "globals.ih"

bool Globals::findSimType(string const &value)
{
    return find(s_label[OPTION], s_label[OPTION + 1], value) !=
                                 s_label[OPTION + 1];
}
