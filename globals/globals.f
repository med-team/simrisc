// static
inline char const *Globals::defaultSimType()
{
    return s_label[OPTION][0];
}

// static
inline bool Globals::isBreast()
{
    return s_simType == BREAST;
}

// static
inline bool Globals::isMale()
{
    return s_simType == MALE;
}

// static
inline bool Globals::isPositiveZero(double value)
{
    return 0 <= value and value <= TOLERANCE;
}

// static
inline char const *Globals::label(SimulationType type)
{
    return s_label[CAPS][type];
}

// static
inline bool Globals::proportion(double const &value)
{
    return 0 <= value and value <= 1;
}

// static
inline void Globals::setVary(bool vary)
{
    s_vary = vary;
}

// static
inline char const *Globals::simTypeLabel(FirstChar fc)
{
    return s_label[fc][s_simType];
}


// static
inline SimulationType Globals::simulationType()
{
    return s_simType;
}

// static
inline bool Globals::vary()
{
    return s_vary;
}
