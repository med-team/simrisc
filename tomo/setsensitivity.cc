//#define XERR
#include "tomo.ih"

void Tomo::setSensitivity()
{
    base().back() = "sensitivity:";

    d_sens = Parser::VSDparameters(VARY_PROB, base(), 4);
}
