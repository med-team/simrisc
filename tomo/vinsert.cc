//#define XERR
#include "tomo.ih"

void Tomo::vInsert(ostream &out) const
{
    VSD::fmt(6, "bi-rad:", 'a', 1, 0, 1, 0);
    out << setw(4) << ' ' << "Dose:\n" <<
           d_dose;

    VSD::fmt(6, "bi-rad:", 'a', 0, 2, 0, 2);
    out << setw(4) << ' ' << "Sensitivity:\n" <<
           d_sens;

    AgeGroupVSD::fmt(6, 0, 3, 0, 4);
    out << setw(4) << ' ' << "Specificity:\n" <<
           d_specVect;
}
