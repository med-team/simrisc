//#define XERR
#include "tomo.ih"

// override
void Tomo::vVary(ostream &out)
{
    out << "  Tomo:\n";

    VSD::vary(out, 4, "Dose:",        "bi-rad", 'a', d_dose);
    VSD::vary(out, 4, "Sensitivity:", "bi-rad", 'a', d_sens);

    AgeGroupVSD::vary(out, 4, "Specificity:", d_specVect);
}
