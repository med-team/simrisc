#define XERR
#include "tomo.ih"

//  Tomo:
//      costs:         64
//
//      #      bi-rad:  a       b       c       d
//      dose:           3       3       3       3
//      sensitivity:    .87     .84     .73     .65
//
//      #             ageGroup
//      specificity:  0 - 40:  .961     40 - *: .965

Tomo::Tomo()
:
    ModBase("Tomo")
{
    if (not defined())
        return;

    doseBase(d_dose);               // using ModBase's member
    setSensitivity();

    specificityBase(d_specVect);
}
