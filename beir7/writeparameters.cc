//#define XERR
#include "beir7.ih"

void Beir7::writeParameters(ostream &out) const
{
    VSD::fmt(1, 2, 1,  2);

    Globals::setWidthPrec(out, 3, 3) <<
           "    Beir7:\n"
           "      " << Globals::simTypeLabel(LC) <<
                    (Globals::simulationType() == MALE ? "  " : "") <<
                    "  eta = " << d_eta << ", beta = " << d_vsd << "\n\n";
}
