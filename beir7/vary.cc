//#define XERR
#include "beir7.ih"

void Beir7::vary(ostream &out)
{
    d_vsd.vary();
    out << "  Beir7:\n"
           "    " << Globals::simTypeLabel(LC) << "  ";
    d_vsd.showVary(out);
}
