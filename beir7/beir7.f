inline double Beir7::beta() const
{
    return d_vsd.value();
}

inline double Beir7::eta() const
{
    return d_eta;
}

inline DistType Beir7::distType() const
{
    return d_vsd.distType();
}

//inline double Beir7::spread() const
//{
//    return d_vsd.spread();
//}
