#ifndef INCLUDED_BEIR7_
#define INCLUDED_BEIR7_

//Tumor:
//        #     eta     beta    spread      dist
//    Beir7:    -2.0    .51     .32         Normal

// Tumor:
//    Beir7:
//            #       eta     beta    spread  dist.
//        breast:     -2.0    0.51    0.32    Normal
//
//            # LC:
//        male:       -1.4     .32            Beta
//        female:     -1.4    1.40            Beta


#include <iosfwd>

#include "../typedefs/typedefs.h"
#include "../vsd/vsd.h"

class Beir7
{
    double d_eta;       // eta;
    VSD d_vsd;          // contains beta and spread/distribution

    public:
        Beir7();

        double beta() const;                                            // .f
        double eta() const;                                             // .f
        DistType distType() const;                                      // .f

        void vary(std::ostream &out);
        void writeParameters(std::ostream &out) const;

        // double spread() const;
};

#include "beir7.f"

#endif
