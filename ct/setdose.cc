#define XERR
#include "ct.ih"

void CT::setDose()
{
    base().back() = "dose:";

    if (not Parser::one(base(), d_dose) or d_dose < 0)
        Err::msg(Err::SPEC_ERROR) << "dose:";
}
