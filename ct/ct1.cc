#define XERR
#include "ct.ih"

// configfile lines:


CT::CT(Tumor const &tumor)
:
    ModBase("CT"),
    d_tumor(tumor),
    d_specificity(VARY_NONNEG)
{
    if (not defined())
        return;

    setDose();
    setSpecificity();
    setSensitivity();
}
