#define XERR
#include "ct.ih"

double CT::vSensitivity(size_t idx) const
{
    double diameter = d_tumor.diameter();

    for (Sensitivity const &sens: d_sensitivity)
    {
        if (sens.group.contains(diameter))
            return (sens.sens != -1 ? sens.sens : sensFun(diameter)) / 100;
    }

    Err::msg(Err::CT_NO_SENS) << diameter << '\n';
    return 100;
}
