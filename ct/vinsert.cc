//#define XERR
#include "ct.ih"

void CT::vInsert(ostream &out) const
{
    VSD::fmt(0, 2, 0, 3);

    for (Sensitivity const &sens: d_sensitivity)
    {
        out << setw(4) << ' ' << "sensitivity:  " <<
                    sens.group << ": ";

        if (sens.sens == -1)
            out << "f(diameter), cf. configuration file\n";
        else
            out << sens.sens << '\n';
    }

    out << setw(4) << ' ' << "specificity:  " << d_specificity << '\n';
}
