#ifndef INCLUDED_CT_
#define INCLUDED_CT_

#include <iosfwd>

#include "../vsd/vsd.h"
#include "../modbase/modbase.h"

//      ModBase uses CT: costs: to read the 'screening' costs.
//      CT reads
//
// CT:
//     #        screening    diagnosis    M0      M1   (M0, M1: Table S3)
//     costs:      176         1908      37909   56556
//
//     dose:       1

//     #               diam.
//     sensitivity:    0 - 3:  0
//     sensitivity:    3 - 5:  -1  # formula: (.5 * diam - 1.5) * 100
//     sensitivity:    5 - *:  100
//
//     #               mean    stddev  dist
//     specificity:    99.2    .076    Normal

class Tumor;

class CT: public ModBase
{
    struct Sensitivity
    {
        Group group;
        int sens;
    };

    double d_dose;

    Tumor const &d_tumor;

    VSD d_specificity;

    std::vector<Sensitivity> d_sensitivity;

    public:
        CT(Tumor const &tumor);
        ~CT() override;                             // currently empty

    private:
        void add(bool *checkRange, LineInfo const &line);

        static double sensFun(double diameter);                         // .ih

        void setDose();
        void setSensitivity();
        void setSpecificity();


        double vDose(uint16_t) const override;
        void vInsert(std::ostream &out) const override;
        double vSensitivity(size_t idx) const override;
        double vSpecificity(double age) const override;
        void vVary(std::ostream &out) override;

        void extract(char const *keyword, VSD &dest);

};


#endif
