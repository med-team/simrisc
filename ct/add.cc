//#define XERR
#include "ct.ih"

// sensitivity:    0 - 3   0   # sensFixed
// sensitivity:    3 - 5   -1  # sensFun: formula: (.5 * diam - 1.5) * 100
// sensitivity:    5 - *   100 # sensFixed

void CT::add(bool *checkRange, LineInfo const &line)
{
    Sensitivity sensitivity{ Group{ RANGE } };

    if (not Parser::extract(line, sensitivity.group, sensitivity.sens))
    {
        *checkRange = false;
        return;
    }

    if (                                            // invalid sens. value
        not (0 <= sensitivity.sens and sensitivity.sens <= 100)
        and sensitivity.sens != -1
    )
    {
        Err::msgTxt(Err::CT_SENS);
        return;
    }

    d_sensitivity.push_back(move(sensitivity));
}
