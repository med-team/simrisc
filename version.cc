#include "main.ih"
#include "icmconf"

#include "VERSION"

namespace Icmake
{
    char version[]  = VERSION;
    char years[]    = YEARS;
    char author[]   = AUTHOR;
}
