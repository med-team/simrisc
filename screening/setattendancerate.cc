//#define XERR
#include "screening.ih"

// attendanceRate:    0.8

void Screening::setAttendanceRate()
{
    d_base.back() = "attendanceRate:";
    Parser::proportion(d_base, d_rate);
}
