//#define XERR
#include "screening.ih"

// called from radiationrisk.cc
// index is the biRad column index to use with BC
// and 0 when used with LC, as LC has no dose variation.


// static
void Screening::errRisk1405(DoubleVect &vect,
                            Modalities const &modalities,
                            Round const &round, uint16_t index,
                            double beta, double eta)
{
                                    // visit all subsequent ages, and compute
                                    // beir7Err separately for each age
                                    // (beir7err2.cc)
    for (uint16_t age = round.rndAge() + 1; age != END_AGE; ++age)
        vect[age] *= beir7Err(age, round, index, beta, eta, modalities);
}
