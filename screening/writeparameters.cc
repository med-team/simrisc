//#define XERR
#include "screening.ih"

void Screening::writeParameters(ostream &out) const
{
    //Globals::setPrecision(out, 3) <<
    out <<
        "Screening:\n"
        "  attendanceRate: " << d_rate << "\n"
        "  rounds:         " << d_roundVect.size() << '\n';

    for (auto const &round: d_roundVect)
        out << "    " << round << '\n';

//    Globals::setPrecision(out, 3).put('\n');
    out.put('\n');

}
