#define XERR
#include "screening.ih"

Screening::Screening(Modalities &modalities)
:
    d_modalities(modalities),
    d_base{ "" },

            // option -e is deprecated and is only available for reasons of
            // backward compatibility.
    d_errRiskVector(Arg::instance().option('e') ? errRisk1404 : errRisk1405),
    d_lungCancer(not Globals::isBreast())
{
    setAttendanceRate();            // from the config file
    setRounds();                    // from the config file
}
