#define XERR
#include "screening.ih"

        // called from loop/initialize.cc

        // indices are Breast bi-rad indices used for the screening round
        // ages, determined by Densities
        // or lung-cancer table S3 indices
        // to use for the ages of the screening rounds

extern size_t g_caseIdx;

DoubleVect Screening::radiationRisk(
                                Modalities const &modalities,
                                Uint16Vect const &indices,
                                double beta, double eta)   // beir7 values
{
    DoubleVect vect = DoubleVect(END_AGE, 1);

//xerr("d_errRiskVector: " << d_errRiskVector);

    g_log << "  Screening::radiationRisk:\n"
             "    rate: " << d_rate << ", beta: " << beta <<
                       ", eta: " << eta << " using " <<
                    (
                        d_errRiskVector == errRisk1405 ?
                            "errRisk1405\n" : "errRisk1404\n"
                    );

    d_attend = BoolVect( d_roundVect.size() );      // 24/1

    for (
        size_t roundIdx = 0, end = d_roundVect.size();
            roundIdx != end;
                ++roundIdx
    )
    {
            // probabilities *lower* than the attendance rate mean the case
            // is attending.
        if (double rnd = Random::instance().uniform(); rnd > d_rate)
            g_log << "    round " << roundIdx << ": rnd = " << rnd <<
                    " > rate: round not attended (" << d_attend[roundIdx] <<
                                                    ")\n";
        else
        {
            d_attend[roundIdx] = true;              // 24/1
            g_log << "    round " << roundIdx << ", rnd = " << rnd <<
                    " <= rate: compute radiation risk\n";

            (*d_errRiskVector)  // errRisk1405, or (deprecated) with option
            (                   //                 -e: errRisk1404
                vect, modalities,
                d_roundVect[roundIdx], indices[roundIdx],
                beta, eta
            );
        }
    }

    return vect;
}
