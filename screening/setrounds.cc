#define XERR
#include "screening.ih"

void Screening::setRounds()
{
    d_base.back() = "round:";

    auto lines = Parser::any(d_base);           // obtain the 'round:' lines

    bool allOK = true;
    LineInfo noneLine;

    while (true)
    {
        LineInfo const *line = lines.get();
        if (line == 0)
            break;

                                                // add a round specification
        allOK = addRound(&noneLine, *line) and allOK;
    }

    if (d_roundVect.size() and not allOK)       // not all rounds were OK
    {
        Err::reset(noneLine);
        Err::msg(Err::ROUND_NONE) << noneLine.txt << endl;
    }
}



