inline bool Screening::attend(size_t idx) const         // 24/1
{
    return d_attend[idx];
}

inline double Screening::rate() const
{
    return d_rate;
}

inline uint16_t Screening::nRounds() const
{
    return d_roundVect.size();
}

inline Round const &Screening::round(size_t idx) const
{
    return d_roundVect[idx];
}
