//#define XERR
#include "screening.ih"

// called from errRisk1405.cc
// index is the biRad column index to use with BC
// and 0 when used with LC, as LC has no dose variation.


// static
double Screening::beir7Err(uint16_t age,
                           Round const &round, uint16_t index, double beta,
                           double eta, Modalities const &modalities)
{
    double risk = 1;

    double factor = beta * pow(age / 60.0, eta) / 1000;

//g_log << age << ' ' << index << ' ' << factor << '\n';

    for (ModBase const *modBase: modalities.use(round.modalityIndices()))
        risk *= (1 + factor * modBase->dose(index));

    return risk;
}
