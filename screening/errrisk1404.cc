//#define XERR
#include "screening.ih"

// (deprecated, used for backward compatibility)
// called from radiationrisk.cc
// index is the biRad column index to use with BC
// and 0 when used with LC, as LC has no dose variation.


// static
void Screening::errRisk1404(DoubleVect &vect,
                            Modalities const &modalities,
                            Round const &round,
                            uint16_t index,
                            double beta, double eta)
{
                                            // compute beir7Err once,
    double error = beir7Err(round, index, beta, eta, modalities);

                                            // then visit all subsequent ages
    for (uint16_t age = round.rndAge() + 1; age != END_AGE; ++age)
        vect[age] *= error;
}
