//#define XERR
#include "screening.ih"

DoubleVect Screening::ages() const
{
    DoubleVect ret;

    for (auto const &round: d_roundVect)
        ret.push_back(round.age());

    return ret;
}
