#define XERR
#include "screening.ih"

    // false: round: none was specified
    // true: round: age modality/ties was specified

bool Screening::addRound(LineInfo *noneLine, LineInfo const &lineInfo)
{
    Round round(d_modalities.modalityIDs());

//xerr(lineInfo.txt);

    auto in = Parser::extract(lineInfo, round);   // extracts the age

    if (round.none())
    {
        *noneLine = lineInfo;               // save the last round: none line
        return false;
    }

    if (not in)
        return true;

    if (not increasingAge(round))           // ages must increase
    {
        Err::msg(Err::ROUND_AGES_DONT_INC) << endl;
        return true;
    }

    if (addModality(in, round))
        d_roundVect.push_back(move(round));

    return true;
}
