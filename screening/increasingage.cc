//#define XERR
#include "screening.ih"

bool Screening::increasingAge(Round const &round) const
{
    return d_roundVect.empty() or
            Globals::weakCompare(d_roundVect.back().age() + 1, round.age())
            != 1;                   // new age must be at least last age + 1
;
}
