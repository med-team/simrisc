#define XERR
#include "tumor.ih"

extern size_t g_caseIdx;
extern size_t g_err;

    // this function computes the estimated death age using the function
    // published in
    // Cancer. 2002 Aug 15;95(4):713-23.  Predicting the survival of patients
    // with breast carcinoma using tumor size.  Michaelson JS, Silverstein M,
    // Wyatt J, Weber G, Moore R, Halpern E, Kopans DB, Hughes K.

void Tumor::setDeathAge(double detectionAge)
{
    requireTumor("death age");

    // interval halving: search between 0 and 100
    double low = 0;
    double high = 100;
    double mid;

    for (size_t iter = 0; iter != N_ITER_AGE; ++iter)
    {
        mid = (low + high) / 2;

//        if (g_caseIdx == 21)
//if (g_caseIdx == 3)
//xerr(mid << ", " << functionF(mid) << ", " << d_pSurvival);

        switch (weakCompare(functionF(mid), d_pSurvival))
        {
            case -1:                // use the lhs half: low..mid
                high = mid;
            continue;

            case 1:                 // use the rhs half: mid::high
                low = mid;
            continue;

            default:                // equal within WEAK_TOLERANCE
            break;
        }

        break;                      // convergence
    }

    d_deathAge = std::min(detectionAge + mid, 100.);


// if (g_caseIdx == 3)
//         xerr("case: " << setw(4) << g_caseIdx << ", self-detection. "
//             "Onset: " << d_onset << ", selfDet.age: " << d_selfDetectAge <<
//             ", extension: " << mid << ", deathAge: " << d_deathAge);

    g_log << "    Tumor::setDeathAge(" << detectionAge << ") = " <<
             d_deathAge << '\n';
}

//    if (g_caseIdx == 21)
//    xerr("diameter: " << d_diameter);

//        if (g_caseIdx == 21)
//        xerr(mid << ", " << functionF(mid) << ", " << d_pSurvival);


////////////////////////////////////////////////////////////////////////
// if (d_deathAge != 100.)
//         xerr("case: " << setw(4) << g_caseIdx << ", self-detection. "
//             "Onset: " << d_onset << ", selfDet.age: " << d_selfDetectAge <<
//             ", extension: " << mid << ", deathAge: " << d_deathAge);







