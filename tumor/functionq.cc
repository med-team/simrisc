#define XERR
#include "tumor.ih"

// For the std. simulations survival[0] and survival[1] are returning the
// a..d values of Survival: type:

// For LC:
// "Supplementary's" tables S3 and S4 are loaded by Survival. and the table
// S4's a..d values are obtained from Survival::op[0..3], see survival.h

extern size_t g_caseIdx;
extern size_t g_err;

double Tumor::functionQ(double years) const
{
    return d_survival[0].value() * pow(years, d_survival[1].value());
}
