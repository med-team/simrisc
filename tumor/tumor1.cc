#define XERR
#include "tumor.ih"

Tumor::Tumor(TumorInfo &tumorInfo)
:
    d_tumorInfo(tumorInfo),
    d_growth(tumorInfo.growth()),
    d_survival(tumorInfo.survival())
    //d_selfDetectAge(-1)
{
//        xerr("d_vsdMatrix");
//        for (size_t idx = 0; idx != 4; ++idx)
//            cerr << d_survival[idx].value() << ' ';
//        cerr << '\n';
}
