#define XERR
#include "tumor.ih"

// if a tumor is present at 'age' then cpt the tumor's diameter and volume at
// 'age', using the age - d_onset time interval

void Tumor::characteristicsAt(double age)
{
    requireTumor("size");

    if ((d_present = (d_onset <= age)))         // if the tumor exists at 
    {                                           // 'age' compute its diam.
        d_diameter =
            diameter(
                d_volume = d_startVolume *      // exp. growth of volume
                           pow(2, (age - d_onset) / d_doublingYears)
            );

        g_log << __FILE__ "      " << age << "\n"
                 "        diameter: " << d_diameter << "\n"
                 "        volume: " << d_volume     << "\n"
                 "        onset : " << d_onset      << "\n"
                 "        tumor lifetime: " << (age - d_onset) << '\n';
    }
    else                                            // no tumor yet at 'age'
    {
        g_log << __FILE__ " No tumor at age " << age << "\n";

        d_volume = 0;
        d_diameter = 0;
        g_log << "      tumor::characteristics: no tumor yet at " << 
                                                                age << '\n';
    }

    d_rowCol = d_survival.setVSD(d_diameter);
}


