//#define XERR
#include "tumor.ih"

ostream &Tumor::insert(ostream &out) const
{
    return
        out <<  
                "     detectable age: " << d_selfDetectAge      << "\n"
                "     pre-period: " << d_prePeriod              << "\n"
                "     onset: "      << d_onset                  << "\n"
                "     doubling days: " << d_doublingDays        << "\n"
                "     doubling years: " << d_doublingYears      << "\n"
                "     p(survival): " << d_pSurvival             << "\n"
                "     start volume: " << d_startVolume;
}

//                "     self detect Diam: " << selfDetectDiam    << "\n"
