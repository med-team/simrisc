#define XERR
#include "tumor.ih"

extern size_t g_caseIdx;
extern size_t g_err;

double Tumor::functionF(double years) const
{
//    d_survival.vsdRow();

//if (g_caseIdx == 3)
//xerr(d_survival[4].value() << ", " << -functionQ(years) << ", " <<
//d_diameter << ",  " << functionZ(years));

    return years <= 0 ?
                1
            :
                d_survival[4].value() * 
                    exp(-functionQ(years)
                                      * pow(d_diameter, functionZ(years)));
}
