//#define XERR
#include "log.ih"

void Log::caseIdx(size_t idx)
{
    if (d_set and (d_active = d_begin <= idx and idx <= d_end))
        d_logFile << "\n"
                    "CASE " << idx << '\n';
}
