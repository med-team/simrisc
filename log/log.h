#ifndef INCLUDED_LOG_
#define INCLUDED_LOG_

#include <iosfwd>
#include <fstream>
#include <string>

class Log
{
    friend Log &nl(Log &log);

    bool d_active;
    unsigned d_begin;
    unsigned d_end;
    bool d_ignore;                          // log is ignored if in d_ignored

                                            // g_log('X') calls are not logged
    std::string d_ignored;                  // if X in d_ignored

    std::ofstream d_logFile;
    bool d_set;

    public:
        Log();
                                            // only interpreted at the
                                            // first call (in d_options)
        void set(unsigned begin, unsigned end, std::string const &fname,
                 std::string const &ignored);

        bool active() const;                // true: logs are produced
        void caseIdx(size_t idx);           // sets d_active if nr in range
        std::ofstream &out();               // d_logFile
        Log &operator()(int ignore);        // if ignored -> no log
};

inline bool Log::active() const
{
    return d_active;
}

inline std::ofstream &Log::out()            // d_logFile
{
    return d_logFile;
}

Log &nl(Log &log);

inline Log &operator<<(Log &log, Log &(*nl)(Log &))
{
    return nl(log);
}

template <typename Type>
Log &operator<<(Log &log, Type &&type)
{
    if (log.active())
        log.out() << std::forward<Type>(type);
    return log;
}

extern Log g_log;

#endif
