//#define XERR
#include "log.ih"

Log &nl(Log &log)
{
    if (log.d_active)
        log.d_logFile.put('\n');
    else if (log.d_ignore)
    {
        log.d_active = true;
        log.d_ignore = false;
    }

    return log;
}
