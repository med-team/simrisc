#ifndef INCLUDED_ROUND_
#define INCLUDED_ROUND_

#include <cmath>
#include <iosfwd>
#include <istream>

    // contains a round specification from the config file

#include "../typedefs/typedefs.h"
#include "../globals/globals.h"

class Round
{
    friend std::istream &operator>>(std::istream &in, Round &round);
    friend std::ostream &operator<<(std::ostream &out, Round const &round);

    double d_age;
    Uint16Vect d_modalities;        // modalities used in this screening round
    StringVect const &d_modalityIDs;

    public:
        Round(StringVect const &modalityIDs);

        bool add(uint16_t idx);

        bool none() const;                      // true: none was specified;

        double age() const;
        uint16_t rndAge() const;                // rounded double age value

                                                // modality indices used by
        Uint16Vect const &modalityIndices() const;  // this screening round

//        StringVect const &modalityIDs() const;  // screening round

    private:
        std::ostream &insert(std::ostream &out) const;
};

inline bool Round::none() const
{
    return Globals::isZero(d_age - END_AGE);
}

inline Round::Round(StringVect const &modalityIDs)
:
    d_modalityIDs(modalityIDs)
{}

inline  Uint16Vect const &Round::modalityIndices() const
{
    return d_modalities;
}

// inline  StringVect const &Round::modalityIDs() const
// {
//     return d_modalities;
// }

inline double Round::age() const
{
    return d_age;
}

inline uint16_t Round::rndAge() const
{
    return round(d_age);
}

inline std::ostream &operator<<(std::ostream &out, Round const &round)
{
    return round.insert(out);
}

typedef std::vector<Round> RoundVect;

#endif
