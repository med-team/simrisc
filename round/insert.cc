//#define XERR
#include "round.ih"

std::ostream &Round::insert(std::ostream &out) const
{
    out << "age: " << static_cast<size_t>(round(d_age));

    for (uint16_t modalityIdx: d_modalities)
        out << ", " << d_modalityIDs[modalityIdx];

    return out;
}
