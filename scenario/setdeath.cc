#define XERR
#include "scenario.ih"

void Scenario::setDeath()
{
    LineInfo const *previous = 0;

    string cumDeath;
    if (cumDeath = Options::instance().cumDeathFile(); not cumDeath.empty())
        readCumDeath(cumDeath);
    else
    {
        d_base.back() = "death:";

        Parser::Lines lines = Parser::any(d_base);

        if (lines.size() == 1)
            readCumDeath(cumDeath = lines.get()->tail);
        else
        {
            cumDeath = "analysis specification";
            LineInfo const *ptr = lines.get();  // skip the first (empty) line
            do
                previous = ptr;                 // keep the previous line
            while ((ptr = lines.get()) and appendCumDeath(ptr->tail));
        }
    }

    checkCumDeath(cumDeath, previous);
}

