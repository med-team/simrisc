#define XERR
#include "scenario.ih"

void Scenario::setNcases()
{
    d_base.back() = "cases:";

    Parser::nonNegative(d_base, d_nCases);
}
