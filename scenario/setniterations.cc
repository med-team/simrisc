//#define XERR
#include "scenario.ih"

void Scenario::setNiterations()
{
    d_base.back() = "iterations:";

    Parser::nonNegative(d_base, d_nIterations);
}
