#ifndef INCLUDED_SCENARIO_
#define INCLUDED_SCENARIO_

#include <iosfwd>

// Scenario:
//   # default values are shown
//
//                               # use true for variable varying
//   spread:                 false
//
//   iterations:             1
//
//                               # random generator behavior:
//                               # random, fixed, increasing
//   generator:              random
//
//                               # initial seed unless using generator: random
//   seed:                   1
//
//                               # n cases to simulate
//   cases:                  1000
//
//   death:                  #death.txt
//     1: .00000 .00000 .00000 .00000 .00000 .00000 .00000 .00000 .00000 .00000
//    11: .00000 .00000 .00000 .00000 .00000 .00000 .00000 .00000 .00000 .00000
//      ... (101 values)

#include "../parser/parser.h"

class Scenario
{
    struct DLine    // offending Death Line specification in case of errors
    {
        std::string const *line;
        size_t lineNr;
    };

    StringVect d_base;

    GeneratorType d_generatorType;

    std::vector<double> d_cumDeathProportions;
    size_t d_nIterations;
    size_t d_nCases;
    size_t d_seed;

    static StringVect s_generatorType;
    static StringVect s_bool;

    public:
        Scenario();

        std::vector<double> const &cumDeathProp() const;

        size_t nIterations() const;
        size_t seed() const;
        GeneratorType generatorType() const;
        size_t nCases() const;

        void writeParameters(std::ostream &out, size_t iter) const;

    private:
        bool appendCumDeath(std::string const &line);
        void checkCumDeath(std::string const &source, 
                           LineInfo const *previous) const;
        void readCumDeath(std::string const &fname);

        void setDeath();                    // set the cum. death proportions
        void setNiterations();              // set n iteratations
        void setNcases();                   // set nCases
        void setSeed();                     // set seed value
        void setGeneratorType();            // set the random generator type
        void setVary();                     // set variableSpread

        static int find(StringVect const &haystack,     // must succeed or
                        std::string const &needle);     // Err::specification
        static LineInfo const *showLineInfo(LineInfo const *ptr);
};

#include "scenario.f"

#endif
