inline std::vector<double> const &Scenario::cumDeathProp() const
{
    return d_cumDeathProportions;
}

inline size_t Scenario::nIterations() const
{
    return d_nIterations;
}

inline size_t Scenario::seed() const
{
    return d_seed;
}

inline GeneratorType Scenario::generatorType() const
{
    return d_generatorType;
}

inline size_t Scenario::nCases() const
{
    return d_nCases;
}
