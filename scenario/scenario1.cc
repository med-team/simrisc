#define XERR
#include "scenario.ih"

Scenario::Scenario()
:
    d_base{ "" }                // 1st field set by set-members
{
    setNiterations();
    setNcases();
    setGeneratorType();
    setSeed();
    setVary();
    setDeath();
}
