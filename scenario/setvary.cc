#define XERR
#include "scenario.ih"

void Scenario::setVary()
{
    d_base.back() = "spread:";

    bool vary = false;

    if (string spread; Parser::one(d_base, spread))
    {
        if (int idx = find(s_bool, spread); idx != -1)
            vary = static_cast<bool>(idx);
    }

    Globals::setVary(vary);
    // xerr("vary = " << Globals::vary());

    Random::initialize();
}
