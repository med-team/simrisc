#define XERR
#include "scenario.ih"

void Scenario::setSeed()
{
    if (d_generatorType == RANDOM_SEED)
    {
        d_seed = 0;
        return;
    }

    d_base.back() = "seed:";

    Parser::nonNegative(d_base, d_seed);
}
